<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     MarketplaceWebService
 *  @copyright   Copyright 2009 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2009-01-01
 */
/******************************************************************************* 

 *  Marketplace Web Service PHP5 Library
 *  Generated: Thu May 07 13:07:36 PDT 2009
 * 
 */

/**
 * Get Report List  Sample
 */

include_once ('.config.inc.php'); 

/************************************************************************
* Uncomment to configure the client instance. Configuration settings
* are:
*
* - MWS endpoint URL
* - Proxy host and port.
* - MaxErrorRetry.
***********************************************************************/
// IMPORTANT: Uncomment the approiate line for the country you wish to
// sell in:
// United States:
$serviceUrl = "https://mws.amazonservices.com";
// United Kingdom
//$serviceUrl = "https://mws.amazonservices.co.uk";
// Germany
//$serviceUrl = "https://mws.amazonservices.de";
// France
//$serviceUrl = "https://mws.amazonservices.fr";
// Italy
//$serviceUrl = "https://mws.amazonservices.it";
// Japan
//$serviceUrl = "https://mws.amazonservices.jp";
// China
//$serviceUrl = "https://mws.amazonservices.com.cn";
// Canada
//$serviceUrl = "https://mws.amazonservices.ca";
// India
//$serviceUrl = "https://mws.amazonservices.in";

$config = array (
  'ServiceURL' => $serviceUrl,
  'ProxyHost' => null,
  'ProxyPort' => -1,
  'MaxErrorRetry' => 3,
);

/************************************************************************
 * Instantiate Implementation of MarketplaceWebService
 * 
 * AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY constants 
 * are defined in the .config.inc.php located in the same 
 * directory as this sample
 ***********************************************************************/
 $service = new MarketplaceWebService_Client(
     AWS_ACCESS_KEY_ID, 
     AWS_SECRET_ACCESS_KEY, 
     $config,
     APPLICATION_NAME,
     APPLICATION_VERSION);
 
/************************************************************************
 * Uncomment to try out Mock Service that simulates MarketplaceWebService
 * responses without calling MarketplaceWebService service.
 *
 * Responses are loaded from local XML files. You can tweak XML files to
 * experiment with various outputs during development
 *
 * XML files available under MarketplaceWebService/Mock tree
 *
 ***********************************************************************/
 // $service = new MarketplaceWebService_Mock();

/************************************************************************
 * Setup request parameters and uncomment invoke to try out 
 * sample for Get Report List Action
 ***********************************************************************/
 // @TODO: set request. Action can be passed as MarketplaceWebService_Model_GetReportListRequest
 // object or array of parameters
// $parameters = array (
//   'Merchant' => MERCHANT_ID,
//   'AvailableToDate' => new DateTime('now', new DateTimeZone('UTC')),
//   'AvailableFromDate' => new DateTime('-6 months', new DateTimeZone('UTC')),
//   'Acknowledged' => false, 
//   'MWSAuthToken' => '<MWS Auth Token>', // Optional
// );
// 
 $request = new MarketplaceWebService_Model_GetReportListRequest($parameters);
 
 $request = new MarketplaceWebService_Model_GetReportListRequest();
 $request->setMerchant(MERCHANT_ID);
 $request->setAvailableToDate(new DateTime('now', new DateTimeZone('UTC')));
 $request->setAvailableFromDate(new DateTime('-3 months', new DateTimeZone('UTC')));
 $request->setAcknowledged(false);
// $request->setMWSAuthToken('<MWS Auth Token>'); // Optional
 
$report_array = invokeGetReportList($service, $request);
/**
  * Get Report List Action Sample
  * returns a list of reports; by default the most recent ten reports,
  * regardless of their acknowledgement status
  *   
  * @param MarketplaceWebService_Interface $service instance of MarketplaceWebService_Interface
  * @param mixed $request MarketplaceWebService_Model_GetReportList or array of parameters
  */
  function invokeGetReportList(MarketplaceWebService_Interface $service, $request) 
  {
      try {
              $response = $service->getReportList($request);


                if ($response->isSetGetReportListResult()) { 
           //         echo("            GetReportListResult\n");
                    $getReportListResult = $response->getGetReportListResult();

                    $reportInfoList = $getReportListResult->getReportInfoList();
                    foreach ($reportInfoList as $reportInfo) {
		    $report_array[] = (array) $reportInfo;             

                    }
                } 

                return($report_array);
     } catch (MarketplaceWebService_Exception $ex) {
  //       echo("Caught Exception: " . $ex->getMessage() . "\n");
  //       echo("Response Status Code: " . $ex->getStatusCode() . "\n");
  //       echo("Error Code: " . $ex->getErrorCode() . "\n");
  //       echo("Error Type: " . $ex->getErrorType() . "\n");
  //       echo("Request ID: " . $ex->getRequestId() . "\n");
  //       echo("XML: " . $ex->getXML() . "\n");
  //       echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
     }
 }

  foreach($report_array as $report_item){
    foreach($report_item as $key => $value){
      foreach($value as $field){
	 if($field['FieldValue'] == '_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_'){ 
	    $return_value = $prev_field['FieldValue']; 
//	    print($return_value);
         }

	 $prev_field = $field;
	 
      }
    }
  }
  
//print_r($return_value);
/************************************************************************
 * Instantiate Implementation of MarketplaceWebService
 * 
 * AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY constants 
 * are defined in the .config.inc.php located in the same 
 * directory as this sample
 ***********************************************************************/
 $service = new MarketplaceWebService_Client(
     AWS_ACCESS_KEY_ID, 
     AWS_SECRET_ACCESS_KEY, 
     $config,
     APPLICATION_NAME,
     APPLICATION_VERSION);
 
/************************************************************************
 * Uncomment to try out Mock Service that simulates MarketplaceWebService
 * responses without calling MarketplaceWebService service.
 *
 * Responses are loaded from local XML files. You can tweak XML files to
 * experiment with various outputs during development
 *
 * XML files available under MarketplaceWebService/Mock tree
 *
 ***********************************************************************/
 // $service = new MarketplaceWebService_Mock();

/************************************************************************
 * Setup request parameters and uncomment invoke to try out 
 * sample for Get Report Action
 ***********************************************************************/
 // @TODO: set request. Action can be passed as MarketplaceWebService_Model_GetReportRequest
 // object or array of parameters

 print_r($report_array);
 $reportId = $return_value;
 
 //print($reportId);
 $parameters = array (
   'Merchant' => MERCHANT_ID,
   'Report' => @fopen('php://memory', 'rw+'),
   'ReportId' => $reportId,
   'MWSAuthToken' => '<MWS Auth Token>', // Optional
 );
 $request = new MarketplaceWebService_Model_GetReportRequest($parameters);

$request = new MarketplaceWebService_Model_GetReportRequest();
$request->setMerchant(MERCHANT_ID);
$request->setReport(@fopen('php://memory', 'rw+'));
$request->setReportId($reportId);
$request->setMWSAuthToken('<MWS Auth Token>'); // Optional
 
$inventory = invokeGetReport($service, $request);
//print_r($inventory);
/**
  * Get Report Action Sample
  * The GetReport operation returns the contents of a report. Reports can potentially be
  * very large (>100MB) which is why we only return one report at a time, and in a
  * streaming fashion.
  *   
  * @param MarketplaceWebService_Interface $service instance of MarketplaceWebService_Interface
  * @param mixed $request MarketplaceWebService_Model_GetReport or array of parameters
  */
  function invokeGetReport(MarketplaceWebService_Interface $service, $request) 
  {
      try {
              $response = $service->getReport($request);
               
                  $product_array[] = (array)  $response->getGetReportResult();          
                  $stream = stream_get_contents($request->getReport());
                  $stream = explode("\n", $stream);
                
                  foreach($stream as $index => $contents){
                  $stream[$index] = preg_split("/[\t]/", $stream[$index]);
		  print_r($stream);
		    for($i = 0; is_string($stream[$index][$i]) && $index > 0; $i++){
		    
		    $products[$index -1][$stream[0][$i]] = $stream[$index][$i];
		    
		    }
                  
                  }
	//	  print_r($products);
                  return($products);
     } catch (MarketplaceWebService_Exception $ex) {
//         echo("Caught Exception: " . $ex->getMessage() . "\n");
//         echo("Response Status Code: " . $ex->getStatusCode() . "\n");
//         echo("Error Code: " . $ex->getErrorCode() . "\n");
//         echo("Error Type: " . $ex->getErrorType() . "\n");
//         echo("Request ID: " . $ex->getRequestId() . "\n");
//         echo("XML: " . $ex->getXML() . "\n");
//         echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
     }
 }
                                                                                
 $request = new FBAInventoryServiceMWS_Model_ListInventorySupplyRequest();
 $request->setSellerId(MERCHANT_ID);
 // object or array of parameters
 invokeListInventorySupply($service, $request);
                                                                                
   function invokeListInventorySupply(FBAInventoryServiceMWS_Interface $service, $request)
  {
      try {
        $response = $service->ListInventorySupply($request);

        echo ("Service Response\n");
        echo ("=============================================================================\n");

        $dom = new DOMDocument();
        $dom->loadXML($response->toXML());
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        echo $dom->saveXML();
        echo("ResponseHeaderMetadata: " . $response->getResponseHeaderMetadata() . "\n");

     } catch (FBAInventoryServiceMWS_Exception $ex) {
        echo("Caught Exception: " . $ex->getMessage() . "\n");
        echo("Response Status Code: " . $ex->getStatusCode() . "\n");
        echo("Error Code: " . $ex->getErrorCode() . "\n");
        echo("Error Type: " . $ex->getErrorType() . "\n");
        echo("Request ID: " . $ex->getRequestId() . "\n");
        echo("XML: " . $ex->getXML() . "\n");
        echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
     }
 }
                                                                               
                                                                                
//print_r($inventory);
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
define('DRUPAL_ROOT', getcwd());
include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'node')
  ->propertyCondition('type', 'product')
//  ->propertyCondition('title', 'your node title')
//  ->propertyCondition('status', 1)
  ->execute();
  
  
$product_query = new EntityFieldQuery();
$product_query->entityCondition('entity_type', 'commerce_product')
   ->entityCondition('type', 'amazon_product')
//   ->propertyCondition('status', 1)
 ;
$result = $product_query->execute();



foreach($result['commerce_product'] as $key => $value){

  $commerce_products[$key] = commerce_product_load($key);

}

print_r($inventory);


 
//print_r($entities);

foreach($entities['node'] as $index => $result){

$nid = $result->nid;
$node[$nid] = node_load($result->nid);
$asin = $node[$nid]->field_amazon_asin;
//print_r($asin['und'][0]['asin']);
  if($asin['und'][0]['asin']){
    $asin_list[$asin['und'][0]['asin']] = $nid;
  }
}

//print_r($asin_list);


foreach($inventory as $index => $values){
	 $price = array( 'und' => array( array('value' => $values['your-price'] , 'safe_value' => $values['your-price']) ) );

    if($asin_list[$values['asin']]){ //find a node
 //	print_r($node[$asin_list[$values['asin']]]); // node exists
	$update_node = $node[$asin_list[$values['asin']]];
//	 print_r($price);
	if($values['afn-fulfillable-quantity'] > 0){  //quantity exists
        print_r($node[$asin_list[$values['asin']]]);

	  if($node[$asin_list[$values['asin']]]->status == 1){  // is published
	    $update_node->title =  $values['product-name'];	 
	    $update_note->field_price = $price;
	    $path = $values['sku'];
	    $update_node->path = array('alias' => $path);
	  }	  
	  else{
	    $update_node->status = 1; //publish it
	    $update_node->title =  $values['product-name'];
	    $update_note->field_price = $price;
	    $path = $values['sku'];
	    $update_node->path = array('alias' => $path);
	  }
	
	}
	else{ // no quantity
	
	  if($node[$asin_list[$values['asin']]]->status == 1){ //is published
	    $update_node->title =  $values['product-name'];
	    $update_node->status = 0; //unpublish it
   	    $update_note->field_price = $price;
	    $path = $values['sku'];
	    $update_node->path = array('alias' => $path);
	  }	  
	  else{
	    $update_node->title =  $values['product-name'];
	    $path = $values['sku'];
    	    $update_note->field_price = $price;
	    $update_node->path = array('alias' => $path);
	  }
	
	} 
    
    }
    else{  // no node found
    
       
    
    $complaint_body = '&nbsp;';
    $update_node = new stdClass();  // Create a new node object
    $update_node->type = 'product';  // Content type
    $update_node->language = LANGUAGE_NONE;  // Or e.g. 'en' if locale is enabled
    node_object_prepare($update_node);  //Set some default values  
    $update_node->title =  $values['product-name'];
    $update_node->body[$update_node->language][0]['value'] = $complaint_body;
    $update_node->body[$update_node->language][0]['summary'] = text_summary($complaint_body);
    $update_node->body[$update_node->language][0]['format'] = 'full_html';
    $update_node->field_amazon_asin['und'][0]['asin'] = $values['asin'];
    $update_note->field_price = $price;

    $update_node->status = 1;   // (1 or 0): published or unpublished
    $update_node->promote = 0;  // (1 or 0): promoted to front page or not
    $update_node->sticky = 0;  // (1 or 0): sticky at top of lists or not
    $update_node->comment = 1;  // 2 = comments open, 1 = comments closed, 0 = comments hidden
    // Add author of the node
    $update_node->uid = 1;
    // Set created date
    $update_node->date = 'complaint_post_date';
    $update_node->created = strtotime('complaint_post_date');
    $path = $values['sku'];
    $update_node->path = array('alias' => $path);
    // Save the node
    node_save($update_node);


    //make a node
    
    }
 // print_r($update_node);
    
   $update_status = node_save($update_node);
}

/*

$con = mysql_connect("localhost","peterlou_peterlou","pef89xes");
if (!$con)
  {
  die('Could not connect: ' . mysql_error());
  }
else
  {
  print 'connected';
  }
mysql_select_db("peterlou_drupal6", $con);


$q0 = "SELECT * FROM `amazoninv`";

$action = mysql_query($q0);



,
while($row = mysql_fetch_array($action)){
//print_r($row);

$name = explode(" ", $row['name']);
if(empty($q3)){
$failed[] = $search;
}
unset($search);
unset($q3);
      foreach($name as $key=>$value){
      if(isset($search)) { $search .= " "; }
      $search .= $value;
      $q1 = "SELECT * FROM `ec_product` AS t1 INNER JOIN `node` AS t2 ON t1.nid = t2.nid WHERE t2.title  LIKE CONVERT( _utf8 '%".$search."%' USING latin1 )";
      $action2 = mysql_query($q1);
      
      
      
	if($row2 = mysql_fetch_array($action2)){
	//print_r($row2);
	//echo '<br/><br/>';
        $result = $row2;
	}
       else { //print $q0;
	//print_r($result); 
	$data[] = $result;
	//echo '<br/><br/>';
       // print mysql_error();
	if(isset($result)){
	$q3 = "UPDATE `ec_product` set `sku` = '".$row['sku']."' WHERE `nid` = '".$result['nid']."'";
        if($action2 = mysql_query($q3)){} else{print mysql_error;}
        

        unset($result);}
       }
}
}

foreach($failed as $key=>$value){
print '<br/><br/>';
print_r($value);
}
*/

?> 
