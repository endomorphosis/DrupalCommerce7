<?php
/** 
 *  PHP Version 5
 *
 *  @category    Amazon
 *  @package     MarketplaceWebService
 *  @copyright   Copyright 2009 Amazon Technologies, Inc.
 *  @link        http://aws.amazon.com
 *  @license     http://aws.amazon.com/apache2.0  Apache License, Version 2.0
 *  @version     2009-01-01
 */
/******************************************************************************* 

 *  Marketplace Web Service PHP5 Library
 *  Generated: Thu May 07 13:07:36 PDT 2009
 * 
 */

/**
 * Get Report List  Sample
 */

include_once ('.config.inc.php'); 

/************************************************************************
* Uncomment to configure the client instance. Configuration settings
* are:
*
* - MWS endpoint URL
* - Proxy host and port.
* - MaxErrorRetry.
***********************************************************************/
// IMPORTANT: Uncomment the approiate line for the country you wish to
// sell in:
// United States:
$serviceUrl = "https://mws.amazonservices.com";
// United Kingdom
//$serviceUrl = "https://mws.amazonservices.co.uk";
// Germany
//$serviceUrl = "https://mws.amazonservices.de";
// France
//$serviceUrl = "https://mws.amazonservices.fr";
// Italy
//$serviceUrl = "https://mws.amazonservices.it";
// Japan
//$serviceUrl = "https://mws.amazonservices.jp";
// China
//$serviceUrl = "https://mws.amazonservices.com.cn";
// Canada
//$serviceUrl = "https://mws.amazonservices.ca";
// India
//$serviceUrl = "https://mws.amazonservices.in";

$config = array (
  'ServiceURL' => $serviceUrl,
  'ProxyHost' => null,
  'ProxyPort' => -1,
  'MaxErrorRetry' => 3,
);

/************************************************************************
 * Instantiate Implementation of MarketplaceWebService
 * 
 * AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY constants 
 * are defined in the .config.inc.php located in the same 
 * directory as this sample
 ***********************************************************************/
 $service = new MarketplaceWebService_Client(
     AWS_ACCESS_KEY_ID, 
     AWS_SECRET_ACCESS_KEY, 
     $config,
     APPLICATION_NAME,
     APPLICATION_VERSION);
 
/************************************************************************
 * Uncomment to try out Mock Service that simulates MarketplaceWebService
 * responses without calling MarketplaceWebService service.
 *
 * Responses are loaded from local XML files. You can tweak XML files to
 * experiment with various outputs during development
 *
 * XML files available under MarketplaceWebService/Mock tree
 *
 ***********************************************************************/
 // $service = new MarketplaceWebService_Mock();

/************************************************************************
 * Setup request parameters and uncomment invoke to try out 
 * sample for Get Report List Action
 ***********************************************************************/
 // @TODO: set request. Action can be passed as MarketplaceWebService_Model_GetReportListRequest
 // object or array of parameters
// $parameters = array (
//   'Merchant' => MERCHANT_ID,
//   'AvailableToDate' => new DateTime('now', new DateTimeZone('UTC')),
//   'AvailableFromDate' => new DateTime('-6 months', new DateTimeZone('UTC')),
//   'Acknowledged' => false, 
//   'MWSAuthToken' => '<MWS Auth Token>', // Optional
// );
// 
 $request = new MarketplaceWebService_Model_GetReportListRequest($parameters);
 
 $request = new MarketplaceWebService_Model_GetReportListRequest();
 $request->setMerchant(MERCHANT_ID);
 $request->setAvailableToDate(new DateTime('now', new DateTimeZone('UTC')));
 $request->setAvailableFromDate(new DateTime('-3 months', new DateTimeZone('UTC')));
 $request->setAcknowledged(false);
// $request->setMWSAuthToken('<MWS Auth Token>'); // Optional
 
$report_array = invokeGetReportList($service, $request);
/**
  * Get Report List Action Sample
  * returns a list of reports; by default the most recent ten reports,
  * regardless of their acknowledgement status
  *   
  * @param MarketplaceWebService_Interface $service instance of MarketplaceWebService_Interface
  * @param mixed $request MarketplaceWebService_Model_GetReportList or array of parameters
  */
  function invokeGetReportList(MarketplaceWebService_Interface $service, $request) 
  {
      try {
              $response = $service->getReportList($request);


                if ($response->isSetGetReportListResult()) { 
           //         echo("            GetReportListResult\n");
                    $getReportListResult = $response->getGetReportListResult();

                    $reportInfoList = $getReportListResult->getReportInfoList();
                    foreach ($reportInfoList as $reportInfo) {
		    $report_array[] = (array) $reportInfo;             

                    }
                } 

                return($report_array);
     } catch (MarketplaceWebService_Exception $ex) {
  //       echo("Caught Exception: " . $ex->getMessage() . "\n");
  //       echo("Response Status Code: " . $ex->getStatusCode() . "\n");
  //       echo("Error Code: " . $ex->getErrorCode() . "\n");
  //       echo("Error Type: " . $ex->getErrorType() . "\n");
  //       echo("Request ID: " . $ex->getRequestId() . "\n");
  //       echo("XML: " . $ex->getXML() . "\n");
  //       echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
     }
 }

  foreach($report_array as $report_item){
    foreach($report_item as $key => $value){
      foreach($value as $field){
	 if($field['FieldValue'] == '_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_'){ 
	    $return_value = $prev_field['FieldValue']; 
//	    print($return_value);
         }

	 $prev_field = $field;
	 
      }
    }
  }
  
//print_r($return_value);
/************************************************************************
 * Instantiate Implementation of MarketplaceWebService
 * 
 * AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY constants 
 * are defined in the .config.inc.php located in the same 
 * directory as this sample
 ***********************************************************************/
 $service = new MarketplaceWebService_Client(
     AWS_ACCESS_KEY_ID, 
     AWS_SECRET_ACCESS_KEY, 
     $config,
     APPLICATION_NAME,
     APPLICATION_VERSION);
 
/************************************************************************
 * Uncomment to try out Mock Service that simulates MarketplaceWebService
 * responses without calling MarketplaceWebService service.
 *
 * Responses are loaded from local XML files. You can tweak XML files to
 * experiment with various outputs during development
 *
 * XML files available under MarketplaceWebService/Mock tree
 *
 ***********************************************************************/
 // $service = new MarketplaceWebService_Mock();

/************************************************************************
 * Setup request parameters and uncomment invoke to try out 
 * sample for Get Report Action
 ***********************************************************************/
 // @TODO: set request. Action can be passed as MarketplaceWebService_Model_GetReportRequest
 // object or array of parameters

 //print_r($report_array);
 $reportId = $return_value;
 
 //print($reportId);
 $parameters = array (
   'Merchant' => MERCHANT_ID,
   'Report' => @fopen('php://memory', 'rw+'),
   'ReportId' => $reportId,
   'MWSAuthToken' => '<MWS Auth Token>', // Optional
 );
 $request = new MarketplaceWebService_Model_GetReportRequest($parameters);

$request = new MarketplaceWebService_Model_GetReportRequest();
$request->setMerchant(MERCHANT_ID);
$request->setReport(@fopen('php://memory', 'rw+'));
$request->setReportId($reportId);
$request->setMWSAuthToken('<MWS Auth Token>'); // Optional
 
$inventory = invokeGetReport($service, $request);

//print_r($inventory);

exit;
/**
  * Get Report Action Sample
  * The GetReport operation returns the contents of a report. Reports can potentially be
  * very large (>100MB) which is why we only return one report at a time, and in a
  * streaming fashion.
  *   
  * @param MarketplaceWebService_Interface $service instance of MarketplaceWebService_Interface
  * @param mixed $request MarketplaceWebService_Model_GetReport or array of parameters
  */
  function invokeGetReport(MarketplaceWebService_Interface $service, $request) 
  {
      try {
              $response = $service->getReport($request);
               
                  $product_array[] = (array)  $response->getGetReportResult();          
                  $stream = stream_get_contents($request->getReport());
                  $stream = explode("\n", $stream);
                
                  foreach($stream as $index => $contents){
                  $stream[$index] = preg_split("/[\t]/", $stream[$index]);
		//  print_r($stream);
			$max = sizeof($stream[$index]);
		    for($i = 0; $i < $max && $index > 0; $i++){
		    
		    $products[$index -1][$stream[0][$i]] = $stream[$index][$i];
		    
		    }
                  
                  }
	//	  print_r($products);
                  return($products);
     } catch (MarketplaceWebService_Exception $ex) {
//         echo("Caught Exception: " . $ex->getMessage() . "\n");
//         echo("Response Status Code: " . $ex->getStatusCode() . "\n");
//         echo("Error Code: " . $ex->getErrorCode() . "\n");
//         echo("Error Type: " . $ex->getErrorType() . "\n");
//         echo("Request ID: " . $ex->getRequestId() . "\n");
//         echo("XML: " . $ex->getXML() . "\n");
//         echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
     }
 }
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
define('DRUPAL_ROOT', getcwd());
include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

function get_terms($term_name){
  $section_vocab = taxonomy_vocabulary_machine_name_load($term_name);
  $efq = new EntityFieldQuery();

  $data['vocabulary'] = $section_vocab;
  $data['tree'] = taxonomy_get_tree($data['vocabulary']->vid, $parent = 0, $max_depth = NULL, $load_entities = false);
  foreach($data['tree'] as $key => $value){
  //print_r($value);
  	$data['terms'][$value->tid] = $value;
		  
  }
  
  return $data;
}


  $query = new EntityFieldQuery();
    $entities = $query->entityCondition('entity_type', 'node')
    ->propertyCondition('type', 'product')
  //  ->propertyCondition('title', 'your node title')
  //  ->propertyCondition('status', 1)
    ->execute();
    



    
  $product_query = new EntityFieldQuery();
  $product_query->entityCondition('entity_type', 'commerce_product')
    ->entityCondition('type', 'amazon_product')
     ->propertyCondition('status', 1);
  $result = $product_query->execute();



  foreach($result['commerce_product'] as $key => $value){

    $commerce_products[$key] = commerce_product_load($key);

  }
print_r($result);




function _find_term($needle, $haystack){
	foreach($haystack['terms'] as $key => $value){
		
		if($value->name == $needle){
			
				return($value->tid);
		
		}
		
	}
	return false;
}





function find_term($term, $vid, $parent = null, $description = null){
	
	//print $term . "++" .$vid . "++" . $parent. "++" .$description."==";

	$query = new EntityFieldQuery;
	  if(empty($description)){
	$result = $query
	  ->entityCondition('entity_type', 'taxonomy_term')
	  ->propertyCondition('name', (string)$term)
	  ->propertyCondition('vid', (string)$vid)
	  ->execute();
	  }
	  else{
	$result = $query
	  ->entityCondition('entity_type', 'taxonomy_term')
	  ->propertyCondition('name', (string)$term)
	  ->propertyCondition('description', (string)$description)
	  ->propertyCondition('vid', (string)$vid)
	  ->execute();	  
		  
	  }
	if(sizeof($result['taxonomy_term']) > 1){	  
		foreach($result['taxonomy_term'] as $key => $value){
			$result['taxonomy_term'][$key] = taxonomy_term_load($value->tid);
			$query = db_select('taxonomy_term_hierarchy', 'tth')
			  ->fields('tth', array('parent'))
			  ->condition('tth.tid', $result['taxonomy_term'][$key]->tid);
			$parents = $query->execute()->fetchCol();
			// Handle single/multiple parents appropriately
			if (count($parents) == 1) {
			  $parents = reset($parents);
			}
			$result['taxonomy_term'][$key]->parent = $parents;
			if($result['taxonomy_term'][$key]->parent == $parent){
			//	print_r($result['taxonomy_term'][$key]);
		//		print($result['taxonomy_term'][$key]->tid  . "\n");

				return($result['taxonomy_term'][$key]->tid);
			}
			$last_result = $result['taxonomy_term'][$key];
		}
		print($last_result->tid . "\n");
		if (!$parent){
			return($last_result->tid);
		}
	}
	else if(sizeof($result['taxonomy_term']) == 1) {
		foreach($result['taxonomy_term'] as $key => $value){
	//		print($result['taxonomy_term'][$key]->tid . "\n");			
			return($result['taxonomy_term'][$key]->tid);			
		}
	}
	else{
	//	print('false'."\n");
		return(false);
	}
}

$brand = get_terms('brand');
$manufacturer = get_terms('manufacturer');
$productgroup = get_terms('productgroup');
$product_type_name = get_terms('producttypename');
$product_categories = get_terms('product_categories');
//$companies = get_terms('companies');


//  print_r($companies['vocabulary']);
 
//print(find_term('Peter Lous', $companies));
 
$query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'node')
  ->propertyCondition('type', 'product')
//  ->propertyCondition('title', 'your node title')
//  ->propertyCondition('status', 1)
  ->execute();


  
 
//print_r($entities);

foreach($entities['node'] as $index => $result){

$nid = $result->nid;
$node[$nid] = node_load($result->nid);
$asin = $node[$nid]->field_amazon_asin;
//print_r($asin['und'][0]['asin']);
  if($asin['und'][0]['asin']){
    $asin_list[$asin['und'][0]['asin']] = $nid;
  }
}



$safe_term_options_list = array_map('check_plain', $term_options_list);

function custom_create_taxonomy_term($name, $vid, $description, $parent_id = null){
	if(!empty($name) && !empty($vid)){	
	  $term = new stdClass();
	  $term->name = $name;
	  $term->vid = $vid;
	  $term->description = $description;
	  if(!empty($parent_id)){
		$term->parent = array($parent_id);
	  }
	  taxonomy_term_save($term);
	  return $term->tid;
	}
 }


function update_product_categories($amazon_node_data){
//	unset($_this_category);
	$product_categories = get_terms('product_categories');
	$this_node = $amazon_node_data->BrowseNode;
	$ancestor_depth = 0;
	$ancestor_tree[$ancestor_depth]['name'] = ($this_node->Name);
	$ancestor_tree[$ancestor_depth]['id'] = ($this_node->BrowseNodeId);
	while(!empty($this_node->Ancestors)){
		$depth = ">>".$depth;
		$ancestor_depth++;
		$ancestor_tree[$ancestor_depth]['name'] =  ($this_node->Ancestors->BrowseNode->Name);
		$ancestor_tree[$ancestor_depth]['id'] =  ($this_node->Ancestors->BrowseNode->BrowseNodeId);
		$this_node = $this_node->Ancestors->BrowseNode;
	}
	
	for($depth = sizeof($ancestor_tree); $depth >= 0; $depth--){
//		print_r($ancestor_tree[$depth]);
		$ancestor_tid = find_term(($ancestor_tree[$depth]['name']), $product_categories['vocabulary']->vid, $parent_tid, ($ancestor_tree[$depth]['id']));
		if($depth ==  sizeof($ancestor_tree)) {
			$root_tid = $ancestor_tid;
		}
		if(empty($ancestor_tid)){
			$ancestor_tid = custom_create_taxonomy_term($ancestor_tree[$depth]['name'], $product_categories['vocabulary']->vid, $ancestor_tree[$depth]['id'], $parent_tid );
			$product_categories = get_terms('product_categories');
			if($parent_tid){
			//	$term = taxonomy_term_load($ancestor_tid);
			//	$term->parent = $parent_tid;
			//	taxonomy_term_save(term);
			}
		}
		$parent_tid = $ancestor_tid;
	}
//	print_r($ancestor_tree);
//	print "\n";
	return($parent_tid);
}
//print_r($asin_list);

//print_r($inventory);
print_r(get_products());

foreach($inventory as $index => $values){
      




}
/*

foreach($inventory as $index => $values){
	$price = array( 'und' => array( array('value' => $values['your-price'] , 'safe_value' => $values['your-price']) ) );
	$amazon_xml = amazon_store_retrieve_item($values['asin']);
//	print_r($amazon_xml);
	$image = array( 'und' => array( array('value' => $amazon_xml->LargeImage->URL , 'safe_value' => $amazon_xml->LargeImage->URL ) ) );

	$_this_Brand = $amazon_xml->ItemAttributes->Brand;
	$_this_Manufacturer = $amazon_xml->ItemAttributes->Manufacturer;
	$_this_Product_type = $amazon_xml->ItemAttributes->Binding;
	$_this_Product_Group = $amazon_xml->ItemAttributes->ProductGroup;
	$_this_Description = $amazon_xml->EditorialReviews->EditorialReview->Content;
	$_nodes = update_product_categories($amazon_xml->BrowseNodes);
	if(!empty($_nodes)){
		print_r($_nodes); print "__node__";
		$product_cateory_tid = array( 'und' => array ( 0 => array( 'tid' => $_nodes)));
	}
	
	$brand_tid = find_term($_this_Brand, $brand['vocabulary']->vid );
	if($brand_tid == false ){
		custom_create_taxonomy_term($_this_Brand, $brand['vocabulary']->vid );
		$brand = get_terms('brand');
		$brand_tid = find_term($_this_Brand, $brand['vocabulary']->vid);
	}
	$brand_tid = array( 'und' => array ( 0 => array( 'tid' => $brand_tid)));
//	print_r($brand_tid);

	$manufacturer_tid = find_term($_this_Manufacturer, $manufacturer['vocabulary']->vid);
	if($manufacturer_tid == false ){
	custom_create_taxonomy_term($_this_Manufacturer, $manufacturer['vocabulary']->vid  );
	$manufacturer = get_terms('manufacturer');
	$manufacturer_tid = find_term($_this_Manufacturer, $manufacturer['vocabulary']->vid);
	}
	$manufacturer_tid = array( 'und' => array ( 0 => array( 'tid' => $manufacturer_tid)));

	
	$product_group_tid = find_term($_this_Product_Group, $productgroup['vocabulary']->vid );
	if($product_group_tid == false ){
	custom_create_taxonomy_term($_this_Product_Group, $productgroup['vocabulary']->vid  );
	$productgroup = get_terms('manufacturer');
	$product_group_tid = find_term($_this_Product_Group, $productgroup['vocabulary']->vid );
	}
	$product_group_tid = array( 'und' => array ( 0 => array( 'tid' => $product_group_tid)));


	$product_type_tid = find_term($_this_Product_type, $product_type_name['vocabulary']->vid);
	if($product_type_tid == false ){
	custom_create_taxonomy_term($_this_Product_type, $product_type_name['vocabulary']->vid  );
	$product_type_name = get_terms('manufacturer');
	$product_type_tid = find_term($_this_Product_type, $product_type_name['vocabulary']->vid);
	}
	$product_type_tid = array( 'und' => array ( 0 => array( 'tid' => $product_type_tid)));
	
	$values['product-name'] = htmlspecialchars(substr($values['product-name'], 0, 127));
	
    if($asin_list[$values['asin']]){ //find a node
		//print_r($node[$asin_list[$values['asin']]]); // node exists
		$update_node = $node[$asin_list[$values['asin']]];
		$update_node->body[$update_node->language][0]['value'] = $complaint_body;
		$update_node->body[$update_node->language][0]['summary'] = text_summary($complaint_body);
		$update_node->body[$update_node->language][0]['format'] = 'full_html';
		$update_node->body['und'][0]['value'] = $_this_Description;
		print($_this_Description . "\n");

	//	print_r($price);
	
    }
    else{  // no node found
       
		$complaint_body = '&nbsp;';
		$update_node = new stdClass();  // Create a new node object
		$update_node->body[$update_node->language][0]['value'] = $complaint_body;
		$update_node->body[$update_node->language][0]['summary'] = text_summary($complaint_body);
		$update_node->body[$update_node->language][0]['format'] = 'full_html';
		$update_node->body['und'][0]['value'] = $_this_Description;
		print($_this_Description . "\n");
		// Save the node
	//	print_r($update_node);

	//	node_save($update_node);


    //make a node
    }
	
			$update_node->type = 'product';  // Content type
		$update_node->language = LANGUAGE_NONE;  // Or e.g. 'en' if locale is enabled
		node_object_prepare($update_node);  //Set some default values  
		$update_node->title =  $values['product-name'];
		$update_node->body[$update_node->language][0]['value'] = $complaint_body;
		$update_node->body[$update_node->language][0]['summary'] = text_summary($complaint_body);
		$update_node->body[$update_node->language][0]['format'] = 'full_html';
		$update_node->body['und'][0]['value'] = $_this_Description;
		print($_this_Description . "\n");
		$update_node->field_amazon_asin['und'][0]['asin'] = $values['asin'];
		$update_node->field_price = $price;
		if(trim($manufacturer_tid['und'][0]['tid']) != '' && !empty(trim($manufacturer_tid['und'][0]['tid']))){
			$update_node->field_manufacturer = $manufacturer_tid;
		}		
		if(trim($product_group_tid['und'][0]['tid']) != '' && !empty(trim($product_group_tid['und'][0]['tid']))){
			$update_node->field_productgroup = $product_group_tid;
		}
		if(trim($product_type_tid['und'][0]['tid']) != '' && !empty(trim($product_type_tid['und'][0]['tid']))){
			$update_node->field_producttypename = $product_type_tid;
		}
		if(trim($brand_tid['und'][0]['tid']) != '' && !empty(trim($brand_tid['und'][0]['tid']))){
			$update_node->field_brand = $brand_tid;
		}
		if(trim($product_cateory_tid['und'][0]['tid']) != '' && !empty(trim($product_cateory_tid['und'][0]['tid']))){
			print_r($product_cateory_tid); print "__product_category__";
			$update_node->field_product_categories = $product_cateory_tid;
		}
		$update_node->field_thumbnail = $image;
	//	$update_node->status = 1;   // (1 or 0): published or unpublished
		$update_node->promote = 0;  // (1 or 0): promoted to front page or not
		$update_node->sticky = 0;  // (1 or 0): sticky at top of lists or not
		$update_node->comment = 1;  // 2 = comments open, 1 = comments closed, 0 = comments hidden
		// Add author of the node
		$update_node->uid = 1;
		// Set created date
		$update_node->date = 'complaint_post_date';
		$update_node->created = strtotime('complaint_post_date');
		$path = $values['sku'];
		$update_node->path = array('alias' => $path);
		if($values['afn-fulfillable-quantity'] > 0){  //quantity exists
		  //  print_r($node[$asin_list[$values['asin']]]);

		  if($node[$asin_list[$values['asin']]]->status == 1){  // is published
			$update_node->title =  $values['product-name'];	 
			$update_note->field_price = $price;
			$path = $values['sku'];
			$update_node->status = 1; 
			$update_node->path = array('alias' => $path);
		  }	  
		  else{
			$update_node->status = 1; //publish it
			$update_node->title =  $values['product-name'];
			$update_note->field_price = $price;
			$path = $values['sku'];
			$update_node->path = array('alias' => $path);
		  }
		
		}
		else{ // no quantity
		
		  if($node[$asin_list[$values['asin']]]->status == 1){ //is published
			$update_node->title =  $values['product-name'];
			$update_node->status = 0; //unpublish it
			$update_note->field_price = $price;
			$path = $values['sku'];
			$update_node->path = array('alias' => $path);
		  }	  
		  else{
			$update_node->title =  $values['product-name'];
			$path = $values['sku'];
			$update_node->status = 0; 
			$update_note->field_price = $price;
			$update_node->path = array('alias' => $path);
		  }
		
		} 
    
//  print_r($values);

//  print_r($update_node);
	$update_node->field_price = $price;
	$update_node->field_thumbnail = $image;
//	print_r($update_node);
	$update_status = node_save($update_node);
}

?> 
