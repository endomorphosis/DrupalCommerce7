ssh <?php

include_once ('.config.inc.php'); 

/************************************************************************
 * Instantiate Implementation of MarketplaceWebService
 * 
 * AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY constants 
 * are defined in the .config.inc.php located in the same 
 * directory as this sample
 ***********************************************************************/
function get_mws_report_by_name($report_name){
  $serviceUrl = "https://mws.amazonservices.com";

  $config = array (
    'ServiceURL' => $serviceUrl,
    'ProxyHost' => null,
    'ProxyPort' => -1,
    'MaxErrorRetry' => 3,
  );
 $service = new MarketplaceWebService_Client(
     AWS_ACCESS_KEY_ID, 
     AWS_SECRET_ACCESS_KEY, 
     $config,
     APPLICATION_NAME,
     APPLICATION_VERSION);

 $request = new MarketplaceWebService_Model_GetReportListRequest();
 $request->setMerchant(MERCHANT_ID);
 $request->setAvailableToDate(new DateTime('now', new DateTimeZone('UTC')));
 $request->setAvailableFromDate(new DateTime('-3 months', new DateTimeZone('UTC')));
 $request->setAcknowledged(false);
 $report_array = invokeGetReportList($service, $request);
  foreach($report_array as $report_item){
    foreach($report_item as $key => $value){
      foreach($value as $field){
	 if($field['FieldValue'] == $report_name){ 
	    $return_value = $prev_field['FieldValue']; 
         }

	 $prev_field = $field;
	 
      }
    }
  }
 // print_r($report_array);
  if(empty($return_value)){
    print('Report '.$report_name.'is not found, generating report'."\n");
    generate_mws_report($report_name);
  }
  else{
    return($return_value);
  }
}

function generate_mws_report($report_name){
  $serviceUrl = "https://mws.amazonservices.com";
  $marketplaceIdArray = array("Id" => array('ATVPDKIKX0DER','A2EUQ1WTGCTBG2'));

  $config = array (
  'ServiceURL' => $serviceUrl,
  'ProxyHost' => null,
  'ProxyPort' => -1,
  'MaxErrorRetry' => 3,
  );

  $service = new MarketplaceWebService_Client(
  AWS_ACCESS_KEY_ID, 
  AWS_SECRET_ACCESS_KEY, 
  $config,
  APPLICATION_NAME,
  APPLICATION_VERSION);
 
 $parameters = array (
   'Merchant' => MERCHANT_ID,
   'MarketplaceIdList' => $marketplaceIdArray,
   'ReportType' => $report_name,
   'ReportOptions' => 'ShowSalesChannel=true',
//   'MWSAuthToken' => '<MWS Auth Token>', // Optional
 );
 
 $request = new MarketplaceWebService_Model_RequestReportRequest($parameters);
 $request = new MarketplaceWebService_Model_RequestReportRequest();
 $request->setMarketplaceIdList($marketplaceIdArray);
 $request->setMerchant(MERCHANT_ID);
 $request->setReportType($report_name);
 $request->setMWSAuthToken('<MWS Auth Token>'); // Optional

// Using ReportOptions
 $request->setReportOptions('ShowSalesChannel=true');
 sleep(60);
 invokeRequestReport($service, $request);
 get_mws_report_details($report_name);
}


  function invokeGetReportList(MarketplaceWebService_Interface $service, $request) 
  {
      try {
              $response = $service->getReportList($request);

                if ($response->isSetGetReportListResult()) { 
           //         echo("            GetReportListResult\n");
                    $getReportListResult = $response->getGetReportListResult();

                    $reportInfoList = $getReportListResult->getReportInfoList();
                    foreach ($reportInfoList as $reportInfo) {
		    $report_array[] = (array) $reportInfo;             

                    }
                } 
                return($report_array);
     } catch (MarketplaceWebService_Exception $ex) {
  //       echo("Caught Exception: " . $ex->getMessage() . "\n");
  //       echo("Response Status Code: " . $ex->getStatusCode() . "\n");
  //       echo("Error Code: " . $ex->getErrorCode() . "\n");
  //       echo("Error Type: " . $ex->getErrorType() . "\n");
  //       echo("Request ID: " . $ex->getRequestId() . "\n");
  //       echo("XML: " . $ex->getXML() . "\n");
  //       echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
     }
 }

function get_mws_report_details($report_name){
  $serviceUrl = "https://mws.amazonservices.com";

  $config = array (
    'ServiceURL' => $serviceUrl,
    'ProxyHost' => null,
    'ProxyPort' => -1,
    'MaxErrorRetry' => 3,
  );
  $service = new MarketplaceWebService_Client(
     AWS_ACCESS_KEY_ID, 
     AWS_SECRET_ACCESS_KEY, 
     $config,
     APPLICATION_NAME,
     APPLICATION_VERSION);

  $reportId = get_mws_report_by_name($report_name);
   
  $parameters = array (
    'Merchant' => MERCHANT_ID,
    'Report' => @fopen('php://memory', 'rw+'),
    'ReportId' => $reportId,
    'MWSAuthToken' => '<MWS Auth Token>', // Optional
  );
  $request = new MarketplaceWebService_Model_GetReportRequest($parameters);

  $request = new MarketplaceWebService_Model_GetReportRequest();
  $request->setMerchant(MERCHANT_ID);
  $request->setReport(@fopen('php://memory', 'rw+'));
  $request->setReportId($reportId);
  $request->setMWSAuthToken('<MWS Auth Token>'); // Optional
  
  $report_details = invokeGetReport($service, $request);
  return($report_details);
}


  function invokeRequestReport(MarketplaceWebService_Interface $service, $request) 
  {
      try {
              $response = $service->requestReport($request);
              
                echo ("Service Response\n");
                echo ("=============================================================================\n");

                echo("        RequestReportResponse\n");
                if ($response->isSetRequestReportResult()) { 
                    echo("            RequestReportResult\n");
                    $requestReportResult = $response->getRequestReportResult();
                    
                    if ($requestReportResult->isSetReportRequestInfo()) {
                        
                        $reportRequestInfo = $requestReportResult->getReportRequestInfo();
                          echo("                ReportRequestInfo\n");
                          if ($reportRequestInfo->isSetReportRequestId()) 
                          {
                              echo("                    ReportRequestId\n");
                              echo("                        " . $reportRequestInfo->getReportRequestId() . "\n");
                          }
                          if ($reportRequestInfo->isSetReportType()) 
                          {
                              echo("                    ReportType\n");
                              echo("                        " . $reportRequestInfo->getReportType() . "\n");
                          }
                          if ($reportRequestInfo->isSetStartDate()) 
                          {
                              echo("                    StartDate\n");
                              echo("                        " . $reportRequestInfo->getStartDate()->format(DATE_FORMAT) . "\n");
                          }
                          if ($reportRequestInfo->isSetEndDate()) 
                          {
                              echo("                    EndDate\n");
                              echo("                        " . $reportRequestInfo->getEndDate()->format(DATE_FORMAT) . "\n");
                          }
                          if ($reportRequestInfo->isSetSubmittedDate()) 
                          {
                              echo("                    SubmittedDate\n");
                              echo("                        " . $reportRequestInfo->getSubmittedDate()->format(DATE_FORMAT) . "\n");
                          }
                          if ($reportRequestInfo->isSetReportProcessingStatus()) 
                          {
                              echo("                    ReportProcessingStatus\n");
                              echo("                        " . $reportRequestInfo->getReportProcessingStatus() . "\n");
                          }
                      }
                } 
                if ($response->isSetResponseMetadata()) { 
                    echo("            ResponseMetadata\n");
                    $responseMetadata = $response->getResponseMetadata();
                    if ($responseMetadata->isSetRequestId()) 
                    {
                        echo("                RequestId\n");
                        echo("                    " . $responseMetadata->getRequestId() . "\n");
                    }
                } 

                echo("            ResponseHeaderMetadata: " . $response->getResponseHeaderMetadata() . "\n");
     } catch (MarketplaceWebService_Exception $ex) {
         echo("Caught Exception: " . $ex->getMessage() . "\n");
         echo("Response Status Code: " . $ex->getStatusCode() . "\n");
         echo("Error Code: " . $ex->getErrorCode() . "\n");
         echo("Error Type: " . $ex->getErrorType() . "\n");
         echo("Request ID: " . $ex->getRequestId() . "\n");
         echo("XML: " . $ex->getXML() . "\n");
         echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
     }
 }




function invokeGetReport(MarketplaceWebService_Interface $service, $request) 
  {
      try {
              $response = $service->getReport($request);
               
                  $product_array[] = (array)  $response->getGetReportResult();          
                  $stream = stream_get_contents($request->getReport());
                  $stream = explode("\n", $stream);
                
                  foreach($stream as $index => $contents){
                  $stream[$index] = preg_split("/[\t]/", $stream[$index]);
		//  print_r($stream);
			$max = sizeof($stream[$index]);
		    for($i = 0; $i < $max && $index > 0; $i++){
		    
		    $report[$index -1][$stream[0][$i]] = $stream[$index][$i];
		    
		    }
                  
                  }
	//	  print_r($products);
                  return($report);
     } catch (MarketplaceWebService_Exception $ex) {
//         echo("Caught Exception: " . $ex->getMessage() . "\n");
//         echo("Response Status Code: " . $ex->getStatusCode() . "\n");
//         echo("Error Code: " . $ex->getErrorCode() . "\n");
//         echo("Error Type: " . $ex->getErrorType() . "\n");
//         echo("Request ID: " . $ex->getRequestId() . "\n");
//         echo("XML: " . $ex->getXML() . "\n");
//         echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
     }
 }
//$inventory = get_mws_report_details('_GET_FBA_FULFILLMENT_CURRENT_INVENTORY_DATA_');
$inventory = get_mws_report_details('_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_');
//$inventory = get_mws_report_details('_GET_MERCHANT_LISTINGS_DATA_');


//print_r($inventory);

 
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
define('DRUPAL_ROOT', getcwd());
include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);


function get_product_variations(){
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'commerce_product')
      ->entityCondition('bundle', 'amazon_product')
//      ->propertyCondition('status', 1)
//      ->fieldOrderBy('commerce_price', 'amount', 'ASC')
    ;
    $result = $query->execute();
      
  foreach($result['commerce_product'] as $key => $value){
    $commerce_products['product_id'][$key] = commerce_product_load($key);
    $commerce_products['asin'][$commerce_products['product_id'][$key]->field_amazon_asin['und'][0]['asin']] =  $key;
    $commerce_products['sku'][$commerce_products['product_id'][$key]->sku] =  $key;
  }
  return($commerce_products);
}

function get_product_nodes(){
    $query = new EntityFieldQuery();
    $entities = $query->entityCondition('entity_type', 'node')
    ->propertyCondition('type', 'amazon_product')
  //  ->propertyCondition('title', 'your node title')
  //  ->propertyCondition('status', 1)
    ->execute();

  foreach($entities['node'] as $index => $result){
    $nid = $result->nid;

    $node_list['nodes'][$nid] = node_load($result->nid);
    if(!empty($node_list['nodes'][$nid]->field_product)){
      $node_list['product_id'][$node_list['nodes'][$nid]->field_product['und'][0]['product_id']] = $nid;
    }
  }
return($node_list);  
}



function get_terms($term_name){
  $section_vocab = taxonomy_vocabulary_machine_name_load($term_name);
  $efq = new EntityFieldQuery();

  $data['vocabulary'] = $section_vocab;
  $data['tree'] = taxonomy_get_tree($data['vocabulary']->vid, $parent = 0, $max_depth = NULL, $load_entities = false);
  foreach($data['tree'] as $key => $value){
  	$data['terms'][$value->tid] = $value;		  
  }
//  print_r($data['vocabulary']);
  return $data;
}

function find_term($term, $vid, $parent = null, $description = null){
	
//	print $term . "++" .$vid . "--" . $parent. "**" .$description."==". "\n";

	$query = new EntityFieldQuery;
	  if(empty($description)){
	$result = $query
	  ->entityCondition('entity_type', 'taxonomy_term')
	  ->propertyCondition('name', (string)$term)
	  ->propertyCondition('vid', (string)$vid)
	  ->execute();
	  }
	  else{
	$result = $query
	  ->entityCondition('entity_type', 'taxonomy_term')
	  ->propertyCondition('name', (string)$term)
	  ->propertyCondition('description', (string)$description)
	  ->propertyCondition('vid', (string)$vid)
	  ->execute();	  
		  
	  }
//	print_r($result);
//	print "\n";
	if(sizeof($result['taxonomy_term']) > 1){	  
		foreach($result['taxonomy_term'] as $key => $value){
			$result['taxonomy_term'][$key] = taxonomy_term_load($value->tid);
			$query = db_select('taxonomy_term_hierarchy', 'tth')
			  ->fields('tth', array('parent'))
			  ->condition('tth.tid', $result['taxonomy_term'][$key]->tid);
			$parents = $query->execute()->fetchCol();
			// Handle single/multiple parents appropriately
			if (count($parents) == 1) {
			  $parents = reset($parents);
			}
			$result['taxonomy_term'][$key]->parent = $parents;
			if($result['taxonomy_term'][$key]->parent == $parent){
			//	print_r($result['taxonomy_term'][$key]);
		//		print($result['taxonomy_term'][$key]->tid  . "\n");

				return($result['taxonomy_term'][$key]->tid);
			}
			$last_result = $result['taxonomy_term'][$key];
		}
		print($last_result->tid . "\n");
		if (!$parent){
			return($last_result->tid);
		}
	}
	else if(sizeof($result['taxonomy_term']) == 1) {
		foreach($result['taxonomy_term'] as $key => $value){
	//		print($result['taxonomy_term'][$key]->tid . "\n");			
			return($result['taxonomy_term'][$key]->tid);			
		}
	}
	else{
	//	print('false'."\n");
		return(false);
	}
}

//$companies = get_terms('companies');


//  print_r($companies['vocabulary']);
 
//print(find_term('Peter Lous', $companies));
 



$safe_term_options_list = array_map('check_plain', $term_options_list);

function custom_create_taxonomy_term($name, $vid, $description, $parent_id = null){
	if(!empty($name) && !empty($vid)){	
	  $term = new stdClass();
	  $term->name = $name;
	  $term->vid = $vid;
	  $term->description = $description;
	  if(!empty($parent_id)){
		$term->parent = array($parent_id);
	  }
	  taxonomy_term_save($term);
	  return $term->tid;
	}
 }
 




function update_product_categories($amazon_node_data){
//	unset($_this_category);
global 	$product_categories;

	$product_categories = get_terms('product_categories');
	$this_node = $amazon_node_data->BrowseNode;
	$ancestor_depth = 0;
	$ancestor_tree[$ancestor_depth]['name'] = ($this_node->Name);
	$ancestor_tree[$ancestor_depth]['id'] = ($this_node->BrowseNodeId);
	while(!empty($this_node->Ancestors)){
		$depth = ">>".$depth;
		$ancestor_depth++;
		$ancestor_tree[$ancestor_depth]['name'] =  ($this_node->Ancestors->BrowseNode->Name);
		$ancestor_tree[$ancestor_depth]['id'] =  ($this_node->Ancestors->BrowseNode->BrowseNodeId);
		$this_node = $this_node->Ancestors->BrowseNode;
	}
	
	for($depth = sizeof($ancestor_tree); $depth >= 0; $depth--){
//		print_r($ancestor_tree[$depth]);
		$ancestor_tid = find_term(($ancestor_tree[$depth]['name']), $product_categories['vocabulary']->vid, $parent_tid, ($ancestor_tree[$depth]['id']));
		if($depth ==  sizeof($ancestor_tree)) {
			$root_tid = $ancestor_tid;
		}
		if(empty($ancestor_tid)){
			$ancestor_tid = custom_create_taxonomy_term($ancestor_tree[$depth]['name'], $product_categories['vocabulary']->vid, $ancestor_tree[$depth]['id'], $parent_tid );
			$product_categories = get_terms('product_categories');
			if($parent_tid){
			//	$term = taxonomy_term_load($ancestor_tid);
			//	$term->parent = $parent_tid;
			//	taxonomy_term_save(term);
			}
		}
		$parent_tid = $ancestor_tid;
	}
//	print "TREE". $parent_tid;
//	print_r($ancestor_tree);
//	print "\n";
	return($parent_tid);
}
//print_r($asin_list);

function format_field_tid($value){
  $new_value['und'][0]['tid'] = $value;
  return $new_value;
}


function create_product_variation($data){


      $product = commerce_product_new('amazon_product');
      $product->sku = $data->sku;
      $product->title = $data->title;
      $product->language = LANGUAGE_NONE;
      $product->field_product_amazon_fulfillment = format_field($data->field_product_amazon_fulfillment);
      $product->field_product_amazon_sku = format_field($data->field_product_amazon_sku);
      $product->field_brand = format_field($data->field_brand);
      $product->field_manufacturer = format_field($data->field_manufacturer);
      $product->field_producttypename = format_field( $data->field_producttypename);
      $product->field_productgroup = format_field( $data->field_productgroup);
      $product->field_product_categories = format_field($data->field_product_categories);
      $product->field_thumbnail = format_field($data->field_thumbnail);
      $product->field_weight = format_field($data->field_weight);
      $product->title_field = format_field($data->title_field);
      $product->uid = 1;
      $product->commerce_price[LANGUAGE_NONE][0] = array(
        'amount' => $data->price, // $10
        'currency_code' => "USD",
      );
      return(commerce_product_save($product));      
}
$brand = get_terms('brand');
$manufacturer = get_terms('manufacturer');
$productgroup = get_terms('productgroup');
$producttypename = get_terms('producttypename');
$product_categories = get_terms('product_categories');
$node_list = get_product_nodes();
$product_variations  = get_product_variations();

//print_r($product_variations);
//print_r($inventory);
//print_r(create_product_variation());
//print_r($node_list);
//exit;
/*
function load_data($type){

$brand = get_terms('brand');
$manufacturer = get_terms('manufacturer');
$productgroup = get_terms('productgroup');
$producttypename = get_terms('producttypename');
$product_categories = get_terms('product_categories');
//$node_list = get_product_nodes();
$product_variations  = get_product_variations();
}
*/


foreach($node_list as $nid => $node){
  $product = commerce_product_load($node->field_product['und'][0]['product_id']);
  print_r($product);   
  
}


foreach($inventory as $index => $values){
//  print_r($values);
  if(!empty($product_variations['asin'][$values['asin']] )|| !empty($product_variations['sku'][$values['sku']])){
    print("++".$product_variations['asin'][$values['asin']]."++"."\n");
    $product_id = $product_variations['asin'][$values['asin']];
    $product_id = $product_variations['sku'][$values['sku']];
    generate_product($values, $product_id);
    }
  else {
    $product_id = generate_product($values);
    print($product_id);  
  }
}



function generate_product($amazon_inventory, $product_id){
  Global $brand;
  Global $manufacturer;
  Global $productgroup;
  Global $producttypename;
  Global $node_list;
  $amazon_xml = amazon_store_retrieve_item($amazon_inventory['asin']);
  $ItemAttributes = process_attributes($amazon_xml->ItemAttributes);
  if($product_id){
  $generate =  commerce_product_load($product_id);
  }
  else{
    $generate = commerce_product_new("amazon_product");
  }
  $amazon_inventory['product-name'] = substr(htmlentities($amazon_inventory['product-name']), 0, 255);
  $generate->field_brand = format_field_tid(check_taxonomy($ItemAttributes->Brand , $brand));
  $generate->field_manufacturer = format_field_tid(check_taxonomy($ItemAttributes->Manufacturer, $manufacturer));
  $generate->field_productgroup = format_field_tid(check_taxonomy($ItemAttributes->ProductGroup, $productgroup));
  $generate->field_producttypename = format_field_tid(check_taxonomy($ItemAttributes->Binding, $producttypename));
  $cateory_hierarchy = update_product_categories($amazon_xml->BrowseNodes);
  if(!empty($cateory_hierarchy)){
    $generate->field_product_categories = array( 'und' => array ( 0 => array( 'tid' => $cateory_hierarchy)));
  }
  $generate->type = 'amazon_product';
  $generate->sku = $amazon_inventory['sku'];
  $generate->title = $amazon_inventory['product-name'];
  $generate->field_amazon_asin['und'][0]['asin'] = $amazon_inventory['asin'];
  if ($amazon_inventory['your-price']){
    $generate->commerce_price['und'][0] = array('amount' => str_replace(".", "", $amazon_inventory['your-price']), 'currency_code' => 'USD');
  }
  $generate->field_product_amazon_sku['und'][0]['value'] = $amazon_inventory['fnsku'];
  $generate->title_original = $amazon_inventory['product-name'];
  $generate->field_features['und'][0] = array('value' =>  implode(" \n", json_decode(json_encode((array)$ItemAttributes->Feature), TRUE)), 'safe_value' => implode("<br/> ", json_decode(json_encode((array)$ItemAttributes->Feature), TRUE))); 
  $generate->title_field['und'][0] = array( 'safe_value' => $amazon_inventory['product-name'], 'value' => $amazon_inventory['product-name']); 
  $generate->field_thumbnail['und'][0] = array('value' => (string) $amazon_xml->LargeImage->URL, 'safe_value' => (string) $amazon_xml->LargeImage->URL); 
 // print_r($generate);
  try{
    commerce_product_save($generate);
  }
  catch(Exception $e) {
    print "error caught on sku".$generate->sku .'product id ='. $product_variations['sku'][$generate->sku];
  }
  $product_variations['sku'][$generate->sku] = $generate->product_id;
  $product_variations['asin'][$generate->field_amazon_asin['und'][0]['asin']] = $generate->product_id;
  $generate->field_product_amazon_fulfillment['und'][0]['value'] = 1;
  if($node_list['product_id'][$generate->product_id]){
    print 'node '. $node_list['product_id'][$generate->product_id] . ' is found as product ID';
    $node = node_load($node_list['product_id'][$generate->product_id]);
    }
  else{  
  print 'node not found for product '.$generate->product_id. ' ' . $node_list['product_id'][$generate->product_id];
  $node = (object)array('type' => 'product_display');
  node_object_prepare($node);
  }
  $node->title = $amazon_inventory['product-name'];
  $node->body['und'][0]['value'] = $string = preg_replace('/[^(\x20-\x7F)]*/','', ( (string) $amazon_xml->EditorialReviews->EditorialReview->Content));
  $node->type =  'amazon_product';
  $node->field_product_categories['und'][0]['tid'] = $cateory_hierarchy;
  $node->uid = 1;

  if($amazon_inventory['afn-fulfillable-quantity'] == 0 || empty($amazon_inventory['your-price']) || $amazon_inventory['your-price'] == 0){   
    $node->status = 0;
  }
  else{
    $node->status = 1; // (1 or 0): published or unpublished
  }
  $node->promote = 0;  // (1 or 0): promoted to front page or not
  $node->sticky = 0;
  $node->field_product[LANGUAGE_NONE][]['product_id'] = $generate->product_id;
  $node->language = LANGUAGE_NONE;
//  print_r($node);
  try{
    node_save($node);
  }
  catch(Exception $e) {
    print 'error in updating node '. $node->nid. "\n";
    print_r($node);
  }
//  print_r($amazon_xml);
//  print_r($amazon_inventory);
//  print_r($category_hierarchy);
  return($generate->product_id);
}

function check_taxonomy($search_term, &$taxonomy_vid ){

  if(!empty($search_term)){
	$term_tid = find_term($search_term, $taxonomy_vid['vocabulary']->vid);
	if($term_tid == false ){
		custom_create_taxonomy_term($search_term, $taxonomy_vid['vocabulary']->vid  );
		$taxonomy_vid = get_terms($taxonomy_vid['vocabulary']->name);
		$term_tid = find_term($search_term,$taxonomy_vid['vocabulary']->vid);
	}
    return($term_tid);
  }
}

function process_attributes($ItemAttributes){
  unset($ItemAttributes->UPCList);
  unset($ItemAttributes->PackageDimensions);
  unset($ItemAttributes->Studio);
  unset($ItemAttributes->Languages);
  unset($ItemAttributes->PublicationDate);
  unset($ItemAttributes->LegalDisclaimer);  
  unset($ItemAttributes->ManufacturerMinimumAge);
  unset($ItemAttributes->ManufacturerMaximumAge);
  unset($ItemAttributes->ItemDimensions);
  unset($ItemAttributes->EANList);
  unset($ItemAttributes->Binding);
  unset($ItemAttributes->CatalogNumberList);
  unset($ItemAttributes->ListPrice);
//  unset($ItemAttributes->Feature);    
  unset($ItemAttributes->PartNumber);
  unset($ItemAttributes->IsAdultProduct);
  unset($ItemAttributes->PackageQuantity);
  unset($ItemAttributes->IsAutographed);
  unset($ItemAttributes->IsMemorabilia);
  unset($ItemAttributes->NumberOfItems);
  unset($ItemAttributes->ReleaseDate);
  unset($ItemAttributes->Warranty);  
  unset($ItemAttributes->Title);
  unset($ItemAttributes->Publisher);
  unset($ItemAttributes->Studio);  
  return($ItemAttributes);
}

?> 
