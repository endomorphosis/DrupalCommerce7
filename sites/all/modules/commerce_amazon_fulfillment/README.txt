*********DIRECTIONS:*********

To implement this module, first make sure you have commerce, and commerce
shipping enabled. Next, enable this module. At that point, proceed to
admin/commerce/config/shipping/amazon-fulfillment There you will need to enter
all of your appropriate credentials with Amazon, along with adjust some
settings as desired.

Next, simple proceed to any newly created products, and you will see on the
product add/edit form the check box for "Fulfilled by amazon". During the
checkout completion phase, all items marked for fulfillment by amazon will be
transmitted to Amazon MWS for fulfillment based on your chosen parameters.

*********REQUIREMENTS:*********
Commerce
Commerce Shipping
