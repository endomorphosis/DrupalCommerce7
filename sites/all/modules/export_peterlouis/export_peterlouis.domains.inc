<?php
/**
 * @file
 * export_peterlouis.domains.inc
 */

/**
 * Implements hook_domain_default_domains().
 */
function export_peterlouis_domain_default_domains() {
  $domains = array();
  $domains['wipe-domain-tables'] = 'wipe-domain-tables';
  $domains['advancednutritionnetwork_com'] = array(
    'subdomain' => 'advancednutritionnetwork.com',
    'sitename' => 'advancednutritionnetwork.com',
    'scheme' => 'http',
    'valid' => 1,
    'weight' => -5,
    'is_default' => 0,
    'machine_name' => 'advancednutritionnetwork_com',
  );
  $domains['advancednutritionnetwork_net'] = array(
    'subdomain' => 'advancednutritionnetwork.net',
    'sitename' => 'advancednutritionnetwork.net',
    'scheme' => 'http',
    'valid' => 1,
    'weight' => -2,
    'is_default' => 0,
    'machine_name' => 'advancednutritionnetwork_net',
  );
  $domains['buy_ta_65direct_com'] = array(
    'subdomain' => 'buy-ta-65direct.com',
    'sitename' => 'http://buy-ta-65direct.com',
    'scheme' => 'http',
    'valid' => 1,
    'weight' => -3,
    'is_default' => 0,
    'machine_name' => 'buy_ta_65direct_com',
  );
  $domains['ghdproduct_com'] = array(
    'subdomain' => 'ghdproduct.com',
    'sitename' => 'GHD Products',
    'scheme' => 'http',
    'valid' => 1,
    'weight' => -12,
    'is_default' => 0,
    'machine_name' => 'ghdproduct_com',
  );
  $domains['greatlengthsofnewyork_com'] = array(
    'subdomain' => 'greatlengthsofnewyork.com',
    'sitename' => 'great lengths of new york',
    'scheme' => 'http',
    'valid' => 1,
    'weight' => -8,
    'is_default' => 0,
    'machine_name' => 'greatlengthsofnewyork_com',
  );
  $domains['http_peterlouissalon_com'] = array(
    'subdomain' => 'peterlouissalon.com',
    'sitename' => 'Peter Louis Salon',
    'scheme' => 'http',
    'valid' => 1,
    'weight' => -13,
    'is_default' => 1,
    'machine_name' => 'http_peterlouissalon_com',
  );
  $domains['jflazartiguehaircare_com'] = array(
    'subdomain' => 'jflazartiguehaircare.com',
    'sitename' => 'J F Lazartigue',
    'scheme' => 'http',
    'valid' => 1,
    'weight' => -7,
    'is_default' => 0,
    'machine_name' => 'jflazartiguehaircare_com',
  );
  $domains['philipbhaircare_com'] = array(
    'subdomain' => 'philipbhaircare.com',
    'sitename' => 'philip b haircare',
    'scheme' => 'http',
    'valid' => 1,
    'weight' => -6,
    'is_default' => 0,
    'machine_name' => 'philipbhaircare_com',
  );
  $domains['phytoamerica_com'] = array(
    'subdomain' => 'phytoamerica.com',
    'sitename' => 'phyto america',
    'scheme' => 'http',
    'valid' => 1,
    'weight' => -9,
    'is_default' => 0,
    'machine_name' => 'phytoamerica_com',
  );
  $domains['renefurtererhaircare_com'] = array(
    'subdomain' => 'renefurtererhaircare.com',
    'sitename' => 'rene furterer haircare',
    'scheme' => 'http',
    'valid' => 1,
    'weight' => -10,
    'is_default' => 0,
    'machine_name' => 'renefurtererhaircare_com',
  );
  $domains['ta_65telomeres_com'] = array(
    'subdomain' => 'ta-65telomeres.com',
    'sitename' => 'ta 65 telomeres',
    'scheme' => 'http',
    'valid' => 1,
    'weight' => -11,
    'is_default' => 0,
    'machine_name' => 'ta_65telomeres_com',
  );
  $domains['www_advancednutritionnetwork_com'] = array(
    'subdomain' => 'www.advancednutritionnetwork.com',
    'sitename' => 'www.advancednutritionnetwork.com',
    'scheme' => 'http',
    'valid' => 1,
    'weight' => -4,
    'is_default' => 0,
    'machine_name' => 'www_advancednutritionnetwork_com',
  );
  $domains['www_advancednutritionnetwork_net'] = array(
    'subdomain' => 'www.advancednutritionnetwork.net',
    'sitename' => 'www.advancednutritionnetwork.net',
    'scheme' => 'http',
    'valid' => 1,
    'weight' => -1,
    'is_default' => 0,
    'machine_name' => 'www_advancednutritionnetwork_net',
  );

  return $domains;
}

/**
 * Implements hook_domain_theme_default_themes().
 */
function export_peterlouis_domain_theme_default_themes() {
  $domain_themes = array();
  $domain_themes['wipe-domain-tables'] = 'wipe-domain-tables';
  $domain_themes['advancednutritionnetwork_com'] = array(
    'advanced' => array(
      'theme' => 'advanced',
      'settings' => array(
        'toggle_logo' => 1,
        'toggle_name' => 0,
        'toggle_slogan' => 1,
        'toggle_node_user_picture' => 1,
        'toggle_comment_user_picture' => 1,
        'toggle_comment_user_verification' => 1,
        'toggle_favicon' => 0,
        'toggle_main_menu' => 1,
        'toggle_secondary_menu' => 1,
        'default_logo' => 0,
        'logo_path' => 'public://advanced_nutrition_1_1.png',
        'default_favicon' => 1,
        'favicon_path' => '',
        'favicon_upload' => '',
        'theme' => 'advanced',
      ),
      'status' => 1,
      'filepath' => 'public://domain-10',
    ),
    'bartik' => array(
      'theme' => 'bartik',
      'settings' => NULL,
      'status' => 0,
      'filepath' => '',
    ),
    'frame' => array(
      'theme' => 'frame',
      'settings' => NULL,
      'status' => 0,
      'filepath' => '',
    ),
    'salon' => array(
      'theme' => 'salon',
      'settings' => NULL,
      'status' => 0,
      'filepath' => NULL,
    ),
  );
  $domain_themes['advancednutritionnetwork_net'] = array(
    'advanced' => array(
      'theme' => 'advanced',
      'settings' => array(
        'toggle_logo' => 1,
        'toggle_name' => 0,
        'toggle_slogan' => 1,
        'toggle_node_user_picture' => 1,
        'toggle_comment_user_picture' => 1,
        'toggle_comment_user_verification' => 1,
        'toggle_favicon' => 1,
        'toggle_main_menu' => 1,
        'toggle_secondary_menu' => 1,
        'default_logo' => 0,
        'logo_path' => 'public://advanced_nutrition_0_1.png',
        'default_favicon' => 1,
        'favicon_path' => '',
        'favicon_upload' => '',
        'theme' => 'advanced',
      ),
      'status' => 1,
      'filepath' => 'public://domain-13',
    ),
  );
  $domain_themes['buy_ta_65direct_com'] = array(
    'advanced' => array(
      'theme' => 'advanced',
      'settings' => NULL,
      'status' => 0,
      'filepath' => '',
    ),
    'ta_65' => array(
      'theme' => 'ta_65',
      'settings' => NULL,
      'status' => 1,
      'filepath' => '',
    ),
  );
  $domain_themes['ghdproduct_com'] = array(
    'ghd' => array(
      'theme' => 'ghd',
      'settings' => array(
        'toggle_logo' => 1,
        'toggle_name' => 0,
        'toggle_slogan' => 0,
        'toggle_node_user_picture' => 1,
        'toggle_comment_user_picture' => 1,
        'toggle_comment_user_verification' => 0,
        'toggle_favicon' => 1,
        'toggle_main_menu' => 1,
        'toggle_secondary_menu' => 1,
        'default_logo' => 0,
        'logo_path' => 'public://header.jpg',
        'logo_upload' => '',
        'default_favicon' => 1,
        'favicon_path' => '',
        'favicon_upload' => '',
        'theme' => 'ghd',
      ),
      'status' => 0,
      'filepath' => 'public://domain-2',
    ),
    'ghdproduct' => array(
      'theme' => 'ghdproduct',
      'settings' => array(
        'toggle_logo' => 1,
        'toggle_name' => 0,
        'toggle_slogan' => 0,
        'toggle_node_user_picture' => 1,
        'toggle_comment_user_picture' => 1,
        'toggle_comment_user_verification' => 1,
        'toggle_favicon' => 1,
        'toggle_main_menu' => 1,
        'toggle_secondary_menu' => 1,
        'default_logo' => 0,
        'logo_path' => 'public://header_5.jpg',
        'default_favicon' => 1,
        'favicon_path' => '',
        'favicon_upload' => '',
        'theme' => 'ghdproduct',
      ),
      'status' => 1,
      'filepath' => 'public://domain-2',
    ),
    'philipb' => array(
      'theme' => 'philipb',
      'settings' => NULL,
      'status' => 0,
      'filepath' => '',
    ),
  );
  $domain_themes['greatlengthsofnewyork_com'] = array(
    'greatlengths' => array(
      'theme' => 'greatlengths',
      'settings' => NULL,
      'status' => 1,
      'filepath' => NULL,
    ),
    'salon' => array(
      'theme' => 'salon',
      'settings' => NULL,
      'status' => 0,
      'filepath' => NULL,
    ),
  );
  $domain_themes['http_peterlouissalon_com'] = array(
    'salon' => array(
      'theme' => 'salon',
      'settings' => NULL,
      'status' => 1,
      'filepath' => NULL,
    ),
  );
  $domain_themes['jflazartiguehaircare_com'] = array(
    'jflazartigue' => array(
      'theme' => 'jflazartigue',
      'settings' => NULL,
      'status' => 1,
      'filepath' => NULL,
    ),
    'salon' => array(
      'theme' => 'salon',
      'settings' => NULL,
      'status' => 0,
      'filepath' => NULL,
    ),
  );
  $domain_themes['philipbhaircare_com'] = array(
    'philipb' => array(
      'theme' => 'philipb',
      'settings' => NULL,
      'status' => 1,
      'filepath' => NULL,
    ),
  );
  $domain_themes['phytoamerica_com'] = array(
    'phyto' => array(
      'theme' => 'phyto',
      'settings' => array(
        'toggle_logo' => 1,
        'toggle_name' => 0,
        'toggle_slogan' => 0,
        'toggle_node_user_picture' => 1,
        'toggle_comment_user_picture' => 1,
        'toggle_comment_user_verification' => 1,
        'toggle_favicon' => 1,
        'toggle_main_menu' => 0,
        'toggle_secondary_menu' => 0,
        'default_logo' => 0,
        'logo_path' => 'public://header_2.jpg',
        'logo_upload' => '',
        'default_favicon' => 1,
        'favicon_path' => '',
        'favicon_upload' => '',
        'theme' => 'phyto',
      ),
      'status' => 1,
      'filepath' => 'public://domain-5',
    ),
    'salon' => array(
      'theme' => 'salon',
      'settings' => NULL,
      'status' => 0,
      'filepath' => NULL,
    ),
  );
  $domain_themes['renefurtererhaircare_com'] = array(
    'renefurterer' => array(
      'theme' => 'renefurterer',
      'settings' => NULL,
      'status' => 1,
      'filepath' => NULL,
    ),
    'salon' => array(
      'theme' => 'salon',
      'settings' => NULL,
      'status' => 0,
      'filepath' => NULL,
    ),
  );
  $domain_themes['ta_65telomeres_com'] = array(
    'ta_65' => array(
      'theme' => 'ta_65',
      'settings' => NULL,
      'status' => 1,
      'filepath' => NULL,
    ),
  );
  $domain_themes['www_advancednutritionnetwork_com'] = array(
    'advanced' => array(
      'theme' => 'advanced',
      'settings' => array(
        'toggle_logo' => 1,
        'toggle_name' => 0,
        'toggle_slogan' => 1,
        'toggle_node_user_picture' => 1,
        'toggle_comment_user_picture' => 1,
        'toggle_comment_user_verification' => 1,
        'toggle_favicon' => 1,
        'toggle_main_menu' => 1,
        'toggle_secondary_menu' => 1,
        'default_logo' => 0,
        'logo_path' => 'public://advanced_nutrition_2_1.png',
        'default_favicon' => 1,
        'favicon_path' => '',
        'favicon_upload' => '',
        'theme' => 'advanced',
      ),
      'status' => 1,
      'filepath' => 'public://domain-11',
    ),
  );
  $domain_themes['www_advancednutritionnetwork_net'] = array(
    'advanced' => array(
      'theme' => 'advanced',
      'settings' => array(
        'toggle_logo' => 1,
        'toggle_name' => 0,
        'toggle_slogan' => 1,
        'toggle_node_user_picture' => 1,
        'toggle_comment_user_picture' => 1,
        'toggle_comment_user_verification' => 1,
        'toggle_favicon' => 1,
        'toggle_main_menu' => 1,
        'toggle_secondary_menu' => 1,
        'default_logo' => 0,
        'logo_path' => 'public://advanced_nutrition_6.png',
        'default_favicon' => 1,
        'favicon_path' => '',
        'favicon_upload' => '',
        'theme' => 'advanced',
      ),
      'status' => 1,
      'filepath' => 'public://domain-14',
    ),
  );

  return $domain_themes;
}
