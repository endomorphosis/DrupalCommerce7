<?php
/**
 * @file
 * export_peterlouis.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function export_peterlouis_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function export_peterlouis_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_menu_position_default_menu_positions().
 */
function export_peterlouis_menu_position_default_menu_positions() {
  $items = array(
    'catalog' => array(
      'admin_title' => 'Catalog',
      'enabled' => 0,
      'conditions' => 'a:1:{s:12:"content_type";a:1:{s:12:"content_type";a:1:{s:7:"catalog";s:7:"catalog";}}}',
      'menu_name' => 'main-menu',
      'plid' => 1489,
      'mlid' => 1428,
      'weight' => 0,
      'machine_name' => 'catalog',
    ),
    'image' => array(
      'admin_title' => 'Image',
      'enabled' => 1,
      'conditions' => 'a:1:{s:12:"content_type";a:1:{s:12:"content_type";a:1:{s:5:"image";s:5:"image";}}}',
      'menu_name' => 'main-menu',
      'plid' => 2061,
      'mlid' => 2067,
      'weight' => 0,
      'machine_name' => 'image',
    ),
    'news' => array(
      'admin_title' => 'News',
      'enabled' => 1,
      'conditions' => 'a:1:{s:12:"content_type";a:1:{s:12:"content_type";a:1:{s:7:"article";s:7:"article";}}}',
      'menu_name' => 'main-menu',
      'plid' => 2147,
      'mlid' => 1385,
      'weight' => 0,
      'machine_name' => 'news',
    ),
    'testimonial' => array(
      'admin_title' => 'Testimonial',
      'enabled' => 1,
      'conditions' => 'a:1:{s:12:"content_type";a:1:{s:12:"content_type";a:1:{s:6:"review";s:6:"review";}}}',
      'menu_name' => 'main-menu',
      'plid' => 2062,
      'mlid' => 2073,
      'weight' => 0,
      'machine_name' => 'testimonial',
    ),
  );
  return $items;
}

/**
 * Implements hook_node_info().
 */
function export_peterlouis_node_info() {
  $items = array(
    'article' => array(
      'name' => t('Media & Press'),
      'base' => 'node_content',
      'description' => t('Articles, news, events'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'blog' => array(
      'name' => t('Blog'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'catalog' => array(
      'name' => t('Catalog'),
      'base' => 'node_content',
      'description' => t('Catalog items on site'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'image' => array(
      'name' => t('Art gallery post'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Page'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'place' => array(
      'name' => t('Place'),
      'base' => 'node_content',
      'description' => t('Office, shop, etc.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'product' => array(
      'name' => t('Product'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'review' => array(
      'name' => t('Testimonials'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'service' => array(
      'name' => t('Service'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'simplenews' => array(
      'name' => t('Simplenews newsletter'),
      'base' => 'node_content',
      'description' => t('A newsletter issue to be sent to subscribed email addresses.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'slider' => array(
      'name' => t('Slider'),
      'base' => 'node_content',
      'description' => t('Slider of some content'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'webform' => array(
      'name' => t('Webform'),
      'base' => 'node_content',
      'description' => t('Create a new form or questionnaire accessible to users. Submission results and statistics are recorded and accessible to privileged users.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
