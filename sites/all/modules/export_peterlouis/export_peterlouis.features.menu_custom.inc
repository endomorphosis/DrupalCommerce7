<?php
/**
 * @file
 * export_peterlouis.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function export_peterlouis_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: devel.
  $menus['devel'] = array(
    'menu_name' => 'devel',
    'title' => 'Development',
    'description' => 'Development link',
  );
  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'Main menu',
  );
  // Exported menu: management.
  $menus['management'] = array(
    'menu_name' => 'management',
    'title' => 'Management',
    'description' => 'The <em>Management</em> menu contains links for administrative tasks.',
  );
  // Exported menu: menu-advanced-nutrition.
  $menus['menu-advanced-nutrition'] = array(
    'menu_name' => 'menu-advanced-nutrition',
    'title' => 'advanced nutrition',
    'description' => '',
  );
  // Exported menu: menu-ghd-menu.
  $menus['menu-ghd-menu'] = array(
    'menu_name' => 'menu-ghd-menu',
    'title' => 'GHD Menu',
    'description' => '',
  );
  // Exported menu: menu-great-lengths-menu.
  $menus['menu-great-lengths-menu'] = array(
    'menu_name' => 'menu-great-lengths-menu',
    'title' => 'Great Lengths Menu',
    'description' => '',
  );
  // Exported menu: menu-j-f-lazartigue.
  $menus['menu-j-f-lazartigue'] = array(
    'menu_name' => 'menu-j-f-lazartigue',
    'title' => 'J F Lazartigue',
    'description' => '',
  );
  // Exported menu: menu-philip-b-menu.
  $menus['menu-philip-b-menu'] = array(
    'menu_name' => 'menu-philip-b-menu',
    'title' => 'Philip B Menu',
    'description' => '',
  );
  // Exported menu: menu-phyto.
  $menus['menu-phyto'] = array(
    'menu_name' => 'menu-phyto',
    'title' => 'Phyto menu',
    'description' => '',
  );
  // Exported menu: menu-rene-menu.
  $menus['menu-rene-menu'] = array(
    'menu_name' => 'menu-rene-menu',
    'title' => 'Rene Menu',
    'description' => '',
  );
  // Exported menu: menu-secondary.
  $menus['menu-secondary'] = array(
    'menu_name' => 'menu-secondary',
    'title' => 'Secondary menu',
    'description' => 'Secondary menu',
  );
  // Exported menu: menu-ta-65-menu.
  $menus['menu-ta-65-menu'] = array(
    'menu_name' => 'menu-ta-65-menu',
    'title' => 'TA-65 menu',
    'description' => '',
  );
  // Exported menu: navigation.
  $menus['navigation'] = array(
    'menu_name' => 'navigation',
    'title' => 'Navigation',
    'description' => 'The <em>Navigation</em> menu contains links intended for site visitors. Links are added to the <em>Navigation</em> menu automatically by some modules.',
  );
  // Exported menu: user-menu.
  $menus['user-menu'] = array(
    'menu_name' => 'user-menu',
    'title' => 'User menu',
    'description' => 'The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Development');
  t('Development link');
  t('GHD Menu');
  t('Great Lengths Menu');
  t('J F Lazartigue');
  t('Main menu');
  t('Management');
  t('Navigation');
  t('Philip B Menu');
  t('Phyto menu');
  t('Rene Menu');
  t('Secondary menu');
  t('TA-65 menu');
  t('The <em>Management</em> menu contains links for administrative tasks.');
  t('The <em>Navigation</em> menu contains links intended for site visitors. Links are added to the <em>Navigation</em> menu automatically by some modules.');
  t('The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.');
  t('User menu');
  t('advanced nutrition');

  return $menus;
}
