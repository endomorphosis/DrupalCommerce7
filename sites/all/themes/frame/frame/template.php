<?php

/**
 * Add body classes if certain regions have content.
 */
function frame_preprocess_html(&$variables) {
  if (!empty($variables['page']['featured'])) {
    $variables['classes_array'][] = 'featured';
  }

  if (!empty($variables['page']['triptych_first'])
    || !empty($variables['page']['triptych_middle'])
    || !empty($variables['page']['triptych_last'])) {
    $variables['classes_array'][] = 'triptych';
  }

  if (!empty($variables['page']['footer_firstcolumn'])
    || !empty($variables['page']['footer_secondcolumn'])
    || !empty($variables['page']['footer_thirdcolumn'])
    || !empty($variables['page']['footer_fourthcolumn'])) {
    $variables['classes_array'][] = 'footer-columns';
  }

  // Add conditional stylesheets for IE
  drupal_add_css(path_to_theme() . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
  drupal_add_css(path_to_theme() . '/css/ie6.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 6', '!IE' => FALSE), 'preprocess' => FALSE));

/**/
	$forie = array(
		'#type' => 'markup',
		'#markup' => '<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->',
	);
	drupal_add_html_head($forie, 'mymodule');
/**/
}

/**
 * Override or insert variables into the page template for HTML output.
 */
function frame_process_html(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_html_alter($variables);
  }
}


/**
 * Override or insert variables into the page template.
 */
function frame_preprocess_page(&$variables) {

/*
	if (arg(0) == 'node' && arg(1) == 'add' && arg(2)) {
    $titles = array(
      'review'     => 'Add testimonial',
    );
    if (isset($titles[arg(2)])) {
      drupal_set_title($titles[arg(2)]);
    }
  }
*/
/*
	if (arg(0)=='node' && is_numeric(arg(1))) {
		$node =  node_load(arg(1));
		if ($node->type == 'portfolio') {
			drupal_set_title(t('Portfolio'));
		}
	}
*/
}

/**
 * Override or insert variables into the page template.
 */
function frame_process_page(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_page_alter($variables);
  }
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
  // Since the title and the shortcut link are both block level elements,
  // positioning them next to each other is much simpler with a wrapper div.
  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
    // Add a wrapper div using the title_prefix and title_suffix render elements.
    $variables['title_prefix']['shortcut_wrapper'] = array(
      '#markup' => '<div class="shortcut-wrapper clearfix">',
      '#weight' => 100,
    );
    $variables['title_suffix']['shortcut_wrapper'] = array(
      '#markup' => '</div>',
      '#weight' => -99,
    );
    // Make sure the shortcut link is the first item in title_suffix.
    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
  }
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function frame_preprocess_maintenance_page(&$variables) {
  if (!$variables['db_is_active']) {
    unset($variables['site_name']);
  }
  drupal_add_css(drupal_get_path('theme', 'frame') . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function frame_process_maintenance_page(&$variables) {
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
}

/**
 * Override or insert variables into the node template.
 */
function frame_preprocess_node(&$variables) {
/*
  $variables['submitted'] = t('published by !username on !datetime', array('!username' => $variables['name'], '!datetime' => $variables['date']));
*/
/*
	// Grab the node object.
	$node = $vars['node'];
	// Make individual variables for the parts of the date.
	$vars['date_day'] = format_date($node->created, 'custom', 'j');
	$vars['date_month'] = format_date($node->created, 'custom', 'M');
	$vars['date_year'] = format_date($node->created, 'custom', 'Y');
*/
/**/
	$node = $variables['node'];
  $variables['node_created'] = format_date($node->created, 'month_year');
/**/
  $variables['submitted'] = t('!datetime', array('!datetime' => $variables['date']));

  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }

}

/**
 * Override or insert variables into the block template.
 */
function frame_preprocess_block(&$variables) {
  // In the header region visually hide block titles.
  if ($variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }
}

/**
 * Implements theme_menu_tree().
 */
function frame_menu_tree($variables) {
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_field__field_type().
 */
function frame_field__taxonomy_term_reference($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<span class="field-label">' . $variables['label'] . ': </span>';
  }

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '">' . $output . '</div>';

  return $output;
}


/**
 * Returns HTML for a breadcrumb trail. Hide "Home" link
 *
 * @param $variables
 *   An associative array containing:
 *   - breadcrumb: An array containing the breadcrumb links.
 */
function frame_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

	//$breadcrumb[] = drupal_get_title();
	$array_size = count($breadcrumb);

  if (!empty($breadcrumb) && $array_size !=1) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= '<div class="breadcrumb">' . implode(' <span class="devider">&rarr;</span> ', $breadcrumb) . '</div>';
    return $output;
  }

}

/**
 * Serch form theming
 *
 */
function frame_form_alter(&$form, &$form_state, $form_id) {

	// Subscribe form
  if ($form_id == 'simplenews_block_form_87') {
    $form['search_block_form']['#title'] = t('Subscribe'); // Change the text on the label element
    $form['search_block_form']['#title_display'] = 'invisible'; // Toggle label visibilty

    $deftext = t('You E-mail');
    $form['search_block_form']['#default_value'] = $deftext; // Set a default value for the textfield
    $form['actions']['submit']['#value'] = t('Submit!'); // Change the text on the submit button

// Add extra attributes to the text box
    $form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = '".$deftext."';}";
    $form['search_block_form']['#attributes']['onfocus'] = "if (this.value == '".$deftext."') {this.value = '';}";
/**/
  }

	// Search form
  if ($form_id == 'search_block_form') {
    $form['search_block_form']['#title'] = t('Search'); // Change the text on the label element
    $form['search_block_form']['#title_display'] = 'invisible'; // Toggle label visibilty
//    $form['search_block_form']['#size'] = 40;  // define size of the textfield
//    $form['search_block_form']['#placeholder'] = t('Search... 1');  

/**/
    $deftext = t('Search...');
    $form['search_block_form']['#default_value'] = $deftext; // Set a default value for the textfield
//    $form['actions']['submit']['#value'] = t('GO!'); // Change the text on the submit button
    $form['actions']['submit']['#value'] = '';

    //$form['actions']['submit'] = array('#type' => 'image_button', '#src' => base_path() . path_to_theme() . '/img/button.png');

// Add extra attributes to the text box
    $form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = '".$deftext."';}";
    $form['search_block_form']['#attributes']['onfocus'] = "if (this.value == '".$deftext."') {this.value = '';}";
/**/
  }
	
	// Add review
	if ($form['#id'] == 'node-form' && $form['type']['#value'] == 'review' && !$form['nid']['#value']){
    drupal_set_title(t('Add testimonial'));
  }

}

/*
function frame_preprocess_search_block_form(&$variables) {
  $variables['search'] = array();
  $hidden = array();
  // Provide variables named after form keys so themers can print each element independently.
  foreach (element_children($variables['form']) as $key) {
    $type = $variables['form'][$key]['#type'];
    if ($type == 'hidden' || $type == 'token') {
      $hidden[] = drupal_render($variables['form'][$key]);
    }
    else {
      $variables['search'][$key] = drupal_render($variables['form'][$key]);
    }
  }
  // Hidden form elements have no value to themers. No need for separation.
  $variables['search']['hidden'] = implode($hidden);
  // Collect all form elements to make it easier to print the whole form.
  $variables['search_form'] = implode($variables['search']);
}
*/


// Create a starting-point query object
/*
    $query = db_select('node')
      ->fields('node', array('nid'))
      ->condition('nid', $node->nid, '!=')
      ->condition('status', 1)
      ->condition('type', $node->type, '=')
      ->range(0, 1);

    $prev = clone $query;
    $list['prev'] = $prev
      ->condition('created', $node->created, '<=')
      ->orderBy('created', 'DESC')
      ->execute()->fetchField();

    $next = clone $query;
    $list['next'] = $next
      ->condition('created', $node->created, '>=')
      ->orderBy('created', 'ASC')
      ->execute()->fetchField();

SELECT node.nid AS nid, node.title AS node_title, field_data_field_order.field_order_value AS field_data_field_order_field_order_value, node.created AS node_created, 'node' AS field_data_field_image_node_entity_type, 'node' AS field_data_field_categories_node_entity_type
FROM 
{node} node
LEFT JOIN {field_data_field_order} field_data_field_order ON node.nid = field_data_field_order.entity_id AND (field_data_field_order.entity_type = 'node' AND field_data_field_order.deleted = '0')
WHERE (( (node.status = '1') AND (node.type IN  ('portfolio')) ))
ORDER BY field_data_field_order_field_order_value ASC, node_created DESC
LIMIT 12 OFFSET 0
$query = db_select('node', 'n');
$query->innerJoin('users', 'u', 'n.uid = u.uid');
$query->fields('n', array('title'));
$query->fields('u', array('name'));
$result = $query->execute();
$next_nid = db_select('node')
	->innerJoin('field_data_field_order', 'order', 'n.nid = order.entity_id')
	->fields('n', 'node', array('nid'))
	->condition('n', 'nid', $node->nid, '!=')
	->condition('n', 'status', 1)
	->condition('n', 'type', $node->type, '=')
	->condition('n', 'created', $node->created, '>=')
	->orderBy('order', 'field_order_value', 'ASC')
	->orderBy('n', 'created', 'ASC')
	->range(0, 1)
	->execute()->fetchField();

*/


function next_page_link($node){
	$next_nid = db_select('node')
		->fields('node', array('nid'))
		->condition('nid', $node->nid, '!=')
		->condition('status', 1)
		->condition('type', $node->type, '=')
		->condition('created', $node->created, '>=')
		->orderBy('created', 'ASC')
		->range(0, 1)
		->execute()->fetchField();
	if($next_nid) {
		return url('node/'.$next_nid);
		//return url('node/'.$next_nid->nid, array('absolute' => TRUE));
	}
	else {
		return false;
	}
}
        
function previous_page_link($node){
	$next_nid = db_select('node')
		->fields('node', array('nid'))
		->condition('nid', $node->nid, '!=')
		->condition('status', 1)
		->condition('type', $node->type, '=')
		->condition('created', $node->created, '<=')
		->orderBy('created', 'DESC')
		->range(0, 1)
		->execute()->fetchField();
	if($next_nid) {
		return url('node/'.$next_nid);
	}
	else {
		return false;
	}
}      


