﻿<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<?php
//dpr($fields['field_image']->content); // выводит масив типа print_r в 

/*dpr($fields['field_image']->content); // выводит масив типа print_r в 
dpr($fields['title']->content); // выводит масив типа print_r в 
dpr($fields['field_categories']->content); // выводит масив типа print_r в 
*/
//dpr($fields['path']->content); // выводит масив типа print_r в 


//dpm($fields['field_categories']); // выводит масив типа print_r в 
//dpm($fields['field_image_fid']); // выводит масив типа print_r в 
/*
[field_image] == Content: Изображение
[title] == Content: Title
[field_categories] == Content: Категория
[path] == Content: Path
<a class="node_link" href="[path]">
  <span class="node_image">[field_image]</span>
  <span class="img_cover"></span>
  <span class="img_area"></span>
  <span class="node_title">[title]</span>
  <span class="node_term">[field_categories]</span>
</a>
*/

?>

<a class="node_link" href="<?php print $fields['path']->content; ?>">
  <span class="node_image"><?php print $fields['field_image']->content; ?></span>
  <span class="node_title"><?php print $fields['title']->content; ?></span>
  <span class="node_term"><?php print $fields['field_categories']->content; ?></span>
</a>


<?php /*
<?php foreach ($fields as $id => $field): ?>
  <?php if (!empty($field->separator)): ?>
    <?php print $field->separator; ?>
  <?php endif; ?>

  <?php print $field->wrapper_prefix; ?>
    <?php print $field->label_html; ?>
    <?php print $field->content; ?>
  <?php print $field->wrapper_suffix; ?>
<?php endforeach; ?>
*/?>