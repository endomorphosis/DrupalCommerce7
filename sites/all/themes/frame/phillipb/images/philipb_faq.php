<? include($template_include_file);?>
<table valign: top; width="720" cellspacing="0" style="vertical-align: top; padding: 0px; margin: 0px; width: 720px; border-width: 0px; font-size: 11px; color: #ffffff;">
<tr style="background-repeat: no-repeat;">
<td height="744" style="  background-image: url('./images/gradient.png'); background-color: #bab8b8; background-repeat: repeat-x; ">
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Q. Are Philip B. products tested on animals?
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
A. Thank you for your caring inquiry. Philip B. Inc., together with our manufacturer, does not perform any animal testing. Philip B. products have not been tested on animals - all testing has been performed on clients, friends, employees, and of course, Philip himself. We do use not animal derived ingredients in our product line.
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Q. Will Drop Dead Straightening Baume work if I just apply it to towel-dried hair?
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
A. No, Drop Dead Straightening Baume is heat-activated by blow-drying with a round or flat brush, or using a roller set with heat. On its own, the Drop Dead Straightening Baume does not straighten the hair.
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Q.What is the difference between the Drop Dead Straightening Baume and a chemical relaxer?
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
A.The Drop Dead Straightening Baume is a safe, gentle product that produces a temporary change. A chemical relaxer is the strongest chemical process that is performed on hair. It is extremely damaging because chemical relaxers usually contain sodium hydroxide (lye) and the changes are permanent.
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Q. Will Drop Dead Straightening Baume work on coarse, kinky hair?
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
A. It does not work as well on coarse, kinky hair as it does on fine to very thick, curly or wavy hair. However, used in conjunction with a straightening blow-dry, flat iron or roller set with heat, it will help hair to straighten a bit more easily.
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Q. What makes PHILIP B shampoo gentle on color?
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
A. Any shampoo with a low pH (5 to 6) - which includes all PHILIP B® shampoos - will be kind to color. There is no way to prevent permanent color from fading, but the more conditioner in the shampoo formula, the gentler it is on color. This is why Scent of Santa Fe Hair & Body Shampoo, with 10% pure plant extracts and shea butter, is gentler than Peppermint and Avocado Shampoo, which has no added conditioner.
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Q. Are cleansing agents like sodium laureth sulfate and ammonium laureth sulfate harmful?
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
A. A cleansing agent is necessary to wash hair thoroughly. A few years ago, a number of articles were written about the supposed toxic action of these ingredients. The Federal Drug Administration and many scientific authorities have affirmed the safety of these ingredients, so long as they are purified, made from natural sources and used in a cosmetic product meant to be rinsed out. Also, bear in mind that cleansing agents lose their harsh effect when formulated with potent moisturizers. All the cleansing agents used in PHILIP B® shampoos are derived naturally (from coconuts) and enhanced by the finest pure botanical extracts.
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
Q. Why is the PHILIP B® Rejuvenating Oil part of the Four Step Hair and Scalp Facial Treatment?
</p>
<p style="color: #ffffff; font-size: 11px; margin-left: 1em; margin-right: 1em;">
A. The scalp is a living organ, with oil glands constantly producing sebum to keep the hair shaft healthy. The roots are the healthiest part of the hair, which gets more dehydrated the further it is from the scalp. Every strand of hair is wrapped in a sheath of stacked, transparent "plates." When these plates dry out, they become opaque and lose their translucency. Oil restores the moisture content, making the plates transparent again - and producing the wonderful shine that says "healthy hair."
</p>
</td>
<td width="200" style="background-image: url('./images/phillipb01.png') ; background-color: #000000; background-repeat: no-repeat;">
</td>
</tr>
</table>