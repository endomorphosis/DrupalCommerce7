<?
//======================================================================================================================
// PRODUCT LIST MODULE
//======================================================================================================================
//---changelog--------------------------------------------------------------------------------------------------------b-
// 03/22/09 - changed to new query
// 03/27/09 - finished functionality for product order
//---changelog--------------------------------------------------------------------------------------------------------e-
//---set globals------------------------------------------------------------------------------------------------------b-
// these should be set in files.php and will pass to this module
//$template_include_file="uniquestylingproducts_type.php";
//$cat_list_title='Unique Styling Products';
//$template_title='Unique Styling Products';
//$cat_num=25;
//$override=false;
//$item_ids="441, 98, 330, 146, 147, 473, 472, 471, 470, 474, 475, 37, 479, 552";
//---set globals------------------------------------------------------------------------------------------------------e-
?>
<table class="bgcolor" width="900" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="padding-left:10px;" valign="top" align="left">
        <table class="bgcolor" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td style="padding-top:6px; padding-bottom:10px; vertical-align:text-top;" align="left"><br><br>
            <font style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; color:#000033;"><?=$cat_list_title;?></font></td>
          </tr>
          <tr>
            <td style="padding-bottom:10px; vertical-align:text-top;" align="left">
				<? include($template_include_file);?></td>
          </tr>
          <tr>
             <td valign="middle" align="left"  height="20">&nbsp;
             	<span class="tableeheader4"> <?=$template_title;?></span>
             </td>
          </tr>
          <tr>
            <td valign="top" align="left">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="bgcolor">                  
<?
if (!$override)
  {				  
  //$cat_num=60;
  //---new query products in category routine---------------------------------------b
  $php_db='item_to_cat';
  $q0="SELECT * FROM ".$php_db." WHERE cat_id='".$cat_num."'";
  $res1=mysql_query($q0);
  //echo $q0;
  $res1_numrow=mysql_num_rows($res1);
  $item_ids='';
  $c=0;
  if ($res1_numrow!=0)
    {
    while ($result=mysql_fetch_assoc($res1))
      { 
      if ($c!=0)
        {
        $item_ids.=', '.$result['item_id'];
        }
        else
        {
        $item_ids.=$result['item_id'];
        }
      //array_push($item_ids,$result['item_id']);
      $c++;
      }
    }
  //  echo ($item_ids);
  //---new query products in category routine---------------------------------------e
  /*
  id  	int(11)  	 	  	No  	 	auto_increment  	  Browse distinct values   	  Change   	  Drop   	  Primary   	  Unique   	  Index   	 Fulltext
	Item_id 	int(11) 			No 	0 		Browse distinct values 	Change 	Drop 	Primary 	Unique 	Index 	Fulltext
	Item_name 	text 	latin1_swedish_ci 		No 			Browse distinct values 	Change 	Drop 	Primary 	Unique 	Index 	Fulltext
	Item_desc 	varchar(1024) 	latin1_swedish_ci 		No 			Browse distinct values 	Change 	Drop 	Primary 	Unique 	Index 	Fulltext
	Item_price 	decimal(20,2) 			No 	0.00 		Browse distinct values 	Change 	Drop 	Primary 	Unique 	Index 	Fulltext
	Item_imageURL 	text 	latin1_swedish_ci 		No 			Browse distinct values 	Change 	Drop 	Primary 	Unique 	Index 	Fulltext
	Item_shopid 	text 	latin1_swedish_ci 		No 			Browse distinct values 	Change 	Drop 	Primary 	Unique 	Index 	Fulltext
	Item_catagory 	varchar(20) 	latin1_swedish_ci 		No 			Browse distinct values 	Change 	Drop 	Primary 	Unique 	Index 	Fulltext
	Item_type 	varchar(255) 	latin1_swedish_ci 		No 			Browse distinct values 	Change 	Drop 	Primary 	Unique 	Index 	Fulltext
	Item_groupid 	int(11) 			No 	0 		Browse distinct values 	Change 	Drop 	Primary 	Unique 	Index 	Fulltext
	Isshow 	varchar(50) 	latin1_swedish_ci 		No 	yes 		Browse distinct values 	Change 	Drop 	Primary 	Unique 	Index 	Fulltext
	sorder
	*/
  $sql="SELECT a1.id, a1.Item_id, a1.Item_name, a1.Item_desc, a1.Item_price, a1.Item_imageURL, a1.Item_shopid, a1.Item_catagory, a1.Item_type, a1.Item_groupid, a1.Isshow, a1.sorder,b2.item_id,b2.cat_id,b2.sort_id";
//  $sql.=" FROM itemdesc as a1";
  $sql.=" FROM item_to_cat AS b2";
//  $sql.=" RIGHT JOIN item_to_cat AS b2 ON (a1.id=b2.item_id ) ";
//  $sql.=" JOIN item_to_cat AS b2 ON b2.item_id=a1.id ";
  $sql.=" JOIN itemdesc as a1 ON b2.item_id=a1.Item_id ";
  //AND b2.cat_id='".$cat_num."'
  //AND item_to_cat.cat_id='".$cat_num."'
  $sql.=" WHERE a1.Item_id in (".$item_ids.") ";
  $sql.=" AND a1.Isshow='"."yes"."' AND a1.Item_imageURL!='http://www.peterlouissalon.com/store/media/none'";
  $sql.=" AND b2.cat_id='".$cat_num."'";
//  $sql.=" AND b2.cat_id='122'";
  //  $sql.=" GROUP BY itemdesc.Item_id ORDER BY id DESC";
  $sql.=" ORDER BY b2.sort_id ASC";
//  $sql.=" GROUP BY itemdesc.Item_id ORDER BY sort_id DESC";
//echo $sql;
  }
  else
  {
  //				  	$items_id="473, 472, 471, 470, 474, 475, 528, 529, 530, 531, 532";
  $sql="select * from itemdesc where Item_id in (".$item_ids.") and Isshow='"."yes"."' AND Item_imageURL!='http://www.peterlouissalon.com/store/media/none' group by itemdesc.Item_id ORDER BY id DESC";
  }      
  
                

  $re=mysql_query($sql);
  $c=mysql_num_rows($re);					
  $i=0;
  if($c > 0)
    {
    while($obj=mysql_fetch_object($re))
      {
      //---pull categories for product and check for category
      $cat_arr_list=unserialize($obj->Item_catagory);
      $cat_template='prod_detail_uniqueSty';
      if (in_array($car_arr_list,'74'))
        {
        $cat_template='prod_detail';
        }

//        .. <a href="template.php?f=prod_detail&ref=rene_uniqueSty&pid=< ?=$obj->id? >" class="bluetext">More Info</a>


                            if($i%2==0)
                            {
                             echo '<tr class="bgcolor">';
                            }?>
                            <td class="bgcolor" valign="top" align="left" style="padding-top:15px; padding-bottom:10px;" width="50%">
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td valign="top" align="left" style="padding-left:6px;" width="42">
                                        <a href="http://www.peterlouissalon.com/template.php?f=prod_detail&ref=rene&pid=<?=$obj->id?>">
                                        <img src="<?=$obj->Item_imageURL?>" alt="<?=$obj->Item_name?>" height="100" vspace="3" hspace="3" border="0" align=left style="border:1px solid #666666;"></a></td>
                                      <td valign="top" align="left" style="padding-right:3px;">
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td valign="top" align="left" colspan="2">
                                             <span style="font-size:8pt; text-decoration: none; font-weight:700">
                                            <a href="http://www.peterlouissalon.com/template.php?f=prod_detail&ref=rene&pid=<?=$obj->id?>">
												<?=$obj->Item_name?></a></span><br>
                                            <br>
                                           
                                            <span style="font-size: 9pt; text-align:justify;"><font color="#000000">
											<? echo substr($obj->Item_desc,0,100); 	?> 
                                            .. <a href="http://www.peterlouissalon.com/template.php?f=prod_detail&ref=rene&pid=<?=$obj->id?>" class="bluetext">More Info</a>
                                            </font></span>                                            
                                            <br />&nbsp;</td>                                          
                                        </tr>
                                        <tr>
                                            <td width="40%" align="left" valign="top"><a href="http://peterlouissalon.com/cgi-bin/sc/order.cgi?storeid=*10cfb3109d06d0e13f85&amp;ref=rene&amp;dbname=products&amp;itemnum=<?=$obj->Item_id?>&amp;function=add"><img border="0" src="images/addtocart.gif" width="70" height="23"></a></td>
                                            <td align="left" valign="top" style="padding-top:4px;">
                                            <font  face="Arial" style="font-size: 8pt" color="#808080"><b>PRICE $ <?=$obj->Item_price?></b></font></td>
                                        </tr>
                                      </table></td>
                                    </tr>
                                  </table>
                              </td>
                        <? 
						 
						if($i%2==1 || $i==($c-1)){ ?>
                             </tr>
                             <tr>
                          		<td class="bgcolor" colspan="2"><hr noshade color="#DBDBDB" size="1"></td>
                          	</tr>
                        <? } 
                        
                         $i++;	
                        } //end while
                    
                    } //end if
                  ?>
          
                </table>
            </td>
          </tr>
        </table>    	
    </td>
  </tr>
</table>
<?
//======================================================================================================================
// PRODUCT LIST MODULE
//======================================================================================================================
?>