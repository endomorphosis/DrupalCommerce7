﻿<?php
// $Id: views-view-fields.tpl.php,v 1.6.6.1 2010/12/04 07:39:35 dereine Exp $
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<?php
//dpr($fields['name']->content); // выводит масив типа print_r в 
//dpm($fields['field_term_tizer']); // выводит масив типа print_r в 
//dpm($fields['field_image_fid']); // выводит масив типа print_r в 
/*
[tid] == Таксономия: ID термина
[name] == Таксономия: Термин
[entity_id_1] == Поля: field_term_tizer
[entity_id] == Поля: field_term_image
*/
?>

<a class="term_link" href="<?php print url('categories/'.$fields['tid']->raw); ?>">
  <span class="term_image">
		<?php print $fields['entity_id']->content; ?>
  </span>
  <span class="term_backgound"></span>
  <span class="term_name"><?php print $fields['name']->content; ?></span>
  <span class="term_tizer"><?php print $fields['entity_id_1']->content; ?></span>
</a>

<?php /*
<?php foreach ($fields as $id => $field): ?>
  <?php if (!empty($field->separator)): ?>
    <?php print $field->separator; ?>
  <?php endif; ?>

  <?php print $field->wrapper_prefix; ?>
    <?php print $field->label_html; ?>
    <?php print $field->content; ?>
  <?php print $field->wrapper_suffix; ?>
<?php endforeach; ?>
*/ ?>
